class ReviewsController < FrontendController
  def create
    if user_signed_in?
      rate = params[:rate]
      obj = rate[:klass].classify.constantize.find(rate[:id])
      obj.rate_new rate[:score].to_f, current_user, rate[:dimension], rate[:review], params[:score]

      redirect_to :back
      flash[:success] = "Review dan rating anda berhasil disimpan!"
    else
      redirect_to :back
      flash[:error] = "Anda haruslah login dahulu, sebelum melakukan penawaran!"
    end
  end

end
