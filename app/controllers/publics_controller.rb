class PublicsController < FrontendController

  def home
    @supporteds = Supported.all
    @testimonials = Testimonial.activated.limit(10)
    @regions = Region.featured.alfa.limit(6)

    @blog_infos = BlogInfo.all.verified.latest.limit(6)
    @event = BlogInfo.bonds.verified.event.latest.limit(5)

    @properties = Property.all.popular_property.package_approved.limit(6)
    @agents = Agent.approved_user.latest.limit(16)
	end

  def package
    @my_account = User.find(current_user) if current_user.present?
  end

  def add_package
    if current_user.present?
      @user = User.find(current_user)
      if @user.update(params_member)
        prepare_package
        UserMailer.add_order_package(@package).deliver
        UserMailer.add_order_package_invoice(@package).deliver
        redirect_to userpage_order_package_path(@package), notice: 'Berhasil, Anda sudah mendaftar berlangganan Paket Agen Premium untuk aktif selama 1 tahun, Silahkan lakukan pembayaran ke rekening yang tersedia dan konfirmasi pembayaran anda! Terimakasih'
      else
        redirect_to :back, alert: 'Error berlangganan!'
      end
    end
  end

  def get_categories
    property_type = PropertyType.find_by(name: params[:name])
    @get_categories = property_type ? property_type.children.alfa : PropertyType.none
    render layout: false
  end

  def get_societies(options={})
    category = params[:category]

    if category
      @facilities = AssetFacility.latest.where("LOWER(category) LIKE LOWER('%#{category}%')")
      render partial: 'publics/facilities_new', layout: false
    end
  end

  def cities_tag
    province = Region.find_by(name: params[:name])
    @cities = province ? province.children : Region.none
    render layout: false
  end

  def contact
  end

  def help
  end

  private
    def params_member
      params.require(:member).permit(contact_attributes: [:handphone, :pin], agent_info_attributes: [:name_company, :specialist_property, :specialist_area, :description, :ktp])
    end

    def prepare_package
      @package = OrderPackage.new
      @package.user_id = current_user.id
      @package.price = 200000
      @package.package_name = 'premium'
      @package.status = false
      @package.save
    end

end
