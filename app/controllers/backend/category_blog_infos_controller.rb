class Backend::CategoryBlogInfosController < Backend::ApplicationController
  defaults resource_class: CategoryBlogInfo, collection_name: 'category_blog_infos', instance_name: 'category_blog_info'
  before_action :role_required, except: [:do_multiple_act]
  
  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Category Blog Info", :collection_path

  include MultipleAction

  def index
    @collection = collection.latest.page(page).per(per_page)
  end

  def edit
    @collection = resource
  end

  protected
    def collection
      @collection ||= end_of_association_chain
    end
end
