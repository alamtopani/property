class Backend::ConfirmationsController < Backend::ApplicationController
  defaults resource_class: Confirmation, collection_name: 'confirmations', instance_name: 'confirmation'

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Confirmations", :collection_path

  include MultipleAction

  def index
    prepare_category
    @collection = collection.latest.page(page).per(per_page)
  end

  def show
    @order = OrderPackage.find_by("LOWER(code) =?", resource.no_invoice.downcase)
  end

  protected
    def collection
      @collection ||= end_of_association_chain.search_by(params)
    end

    def prepare_category
      @category = {'daftar baru' => 'daftar_baru','perpanjang paket' => 'perpanjang_paket'}
    end
end
