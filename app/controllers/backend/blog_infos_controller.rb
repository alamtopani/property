class Backend::BlogInfosController < Backend::ApplicationController
  defaults resource_class: BlogInfo, collection_name: 'blog_infos', instance_name: 'blog_info'
  before_action :role_required, except: [:do_multiple_act]
  
  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Blog Info", :collection_path

  include MultipleAction

  def index
    @collection = collection.latest.page(page).per(per_page)
  end

  protected
    def collection
      @collection ||= end_of_association_chain
    end
end
