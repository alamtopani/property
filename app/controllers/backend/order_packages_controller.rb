class Backend::OrderPackagesController < Backend::ApplicationController
  defaults resource_class: OrderPackage, collection_name: 'order_packages', instance_name: 'order_package'

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Order Packages", :collection_path

  include MultipleAction

  def index
    prepare_status
    @ballance = collection.pluck(:price).sum
    @collection = collection.page(page).per(per_page)
  end

  def create
    build_resource
    create! do |format|
      if resource.errors.empty?
        format.html {redirect_to collection_path}
      else
        flash[:errors] = resource.errors.full_messages
        format.html {redirect_to :back}
      end
    end
  end

  def update
    resource.staf_id = current_user.id
    update! do |format|
      if resource.errors.empty?
        if resource.user.agent_verify? && (resource.user.expired_at < Date.today && params[:order_package][:status] == '1')
          prepare_approved
          UserMailer.send_user_approve_to_verified_extension(resource.user).deliver
        else
          prepare_approved
          UserMailer.send_user_approve_to_verified(resource.user).deliver
        end
        format.html {redirect_to collection_path}
      else
        flash[:errors] = resource.errors.full_messages
        format.html {redirect_to :back}
      end
    end
  end

  def show
    @confirmation = Confirmation.latest.where("LOWER(no_invoice) =?", resource.code.downcase).first
  end

  protected
    def collection
      @collection ||= end_of_association_chain.latest.search_by(params)
    end

    def prepare_approved
      @user = resource.user
      @user.expired_at = Date.today+1.year
      @user.package = 'premium'
      @user.limit = 100
      @user.type = 'Agent'
      @user.verified = true
      @user.save
    end

    def prepare_status
      @status = {'New' => false, 'Paid' => true, 'Expired' => 'expired'}
    end
end
