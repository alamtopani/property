class Backend::LandingPagesController < Backend::ApplicationController
  defaults resource_class: LandingPage, collection_name: 'landing_pages', instance_name: 'landing_page'
  before_action :role_required, except: [:do_multiple_act]

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Landing Pages", :collection_path

  def index
    @collection = collection.latest.page(page).per(per_page)
  end

  protected
    def collection
      @collection ||= end_of_association_chain
    end
end
