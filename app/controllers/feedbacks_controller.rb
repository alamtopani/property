class FeedbacksController < FrontendController

	def create
		@feedback = Feedback.new(permitted_params)

		respond_to do |format|
      if @feedback.save
      	UserMailer.send_feedback(@feedback).deliver
        format.html {redirect_to :back, notice: 'Feedback Kiriman Anda Berhasil di Kirim!'}
      else
        format.html {redirect_to :back, errors: @feedback.errors.full_messages}
      end
    end
	end

	private

		def permitted_params
			params.require(:feedback).permit(Permitable.controller(params[:controller]))
		end
end
