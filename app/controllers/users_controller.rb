class UsersController < FrontendController
  layout 'detail'

  def show
    user
  	@lists = properties.latest.page(page).per(per_page)
  end

  def about_company
    user
  end

  protected
    def user
      @user ||= User.find(params[:id])
    end

    def properties
      @properties = user.properties.activated.package_approved
    end
end
