class ConfirmationsController < FrontendController
  def new
    @confirmation = Confirmation.new
  end

  def create
    check_invoice(params[:confirmation][:no_invoice])

    if @invoice.present?
      @confirmation = Confirmation.new(permitted_params)
      @confirmation.user_id = current_user.id
      if @confirmation.save
        UserMailer.send_confirmation(@confirmation).deliver
        redirect_to :back, notice: 'Konfirmasi Berhasil, Anda sudah mengirim laporan konfirmasi pembayaran paket untuk berlangganan Paket Agen Premium untuk aktif selama 1 tahun, Team kami akan segera memverifikasi pembayaran anda! Verifikasi membutuhkan waktu 1x24 Jam Terimakasih!'
      else
        flash[:errors] = @confirmation.errors.full_messages
        redirect_to :back
      end
    else
      redirect_to :back, alert: 'No Invoice tidak ditemukan!'
    end
  end

  private
    def permitted_params
      params.require(:confirmation).permit(Permitable.controller(params[:controller]))
    end

    def check_invoice(no_invoice)
      @invoice = OrderPackage.find_by("LOWER(code) =?", no_invoice.downcase)
    end
end
