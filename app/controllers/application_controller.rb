class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include DeviseFilter

  before_filter :prepare_checked
  before_filter :landing_page
  before_filter :web_setting

  protected
    def per_page
      params[:per_page] ||= 20
    end

    def page
      params[:page] ||= 1
    end

    def web_setting
	    @setting = WebSetting.first
	  end

    def prepare_checked
      if current_user
        @latest_inquiries = Inquiry.available.received(current_user.id)
      end
    end

    def landing_page
      @landing_page_footer = LandingPage.latest.where.not("category =?", "help")
      @helps   = LandingPage.oldest.where("category =?", "help")
    end

end
