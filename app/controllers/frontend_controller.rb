class FrontendController < ApplicationController
  after_filter :track_action

  protected
    def track_action
      ahoy.track "Processed #{controller_name}##{action_name}", request.filtered_parameters
    end
end
