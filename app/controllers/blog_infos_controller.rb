class BlogInfosController < ApplicationController
  add_breadcrumb "Home", :blog_infos_path
  layout 'blog'

  before_action :prepare_action

  def index
    @blogs      = BlogInfo.verified.latest

    @blog_infos = @blogs.limit(1)
    @info       = @blogs.bonds.info.latest.limit(4)
    @tips       = @blogs.bonds.tips.latest.limit(6)
    @event      = @blogs.bonds.event.latest.limit(3)
    @featureds  = @blogs.featured.limit(7)
    @headlines  = BlogInfo.verified.headline.limit(8)
  end

  def category
    add_breadcrumb "Blog"
    add_breadcrumb "#{category_blog_info.name}"

    category_blog_info
    @blogs = category_blog_info.blog_infos.verified.latest
    @blog_count = @blogs.size
    @results = @blogs.page(page).per(per_page)

    @blogs     = BlogInfo.verified.latest
    @featureds = @blogs.featured.limit(7)
    @headlines = BlogInfo.verified.headline.limit(8)
  end

  def show
    add_breadcrumb "#{resource.category_blog_info.name}"
    add_breadcrumb "#{resource.title}"

    resource
    prepare_quick_count(resource)

    @blogs           = BlogInfo.verified.latest
    @featureds       = @blogs.featured.limit(7)
    @latest_articles = @blogs.limit(4)
    @headlines       = BlogInfo.verified.headline.limit(8)
  end

  def results
    add_breadcrumb "Blog"
    add_breadcrumb "Results"

    blogs = BlogInfo.verified.latest
    @blogs = blogs.search_by(params)
    @blog_count = @blogs.size
    @results = @blogs.page(page).per(per_page)

    @featureds = blogs.featured.limit(6)
    @headlines = BlogInfo.verified.headline.limit(8)
  end

  private
    def resource
      @blog_info = BlogInfo.find params[:id]
    end

    def category_blog_info
      @category_blog_info = CategoryBlogInfo.find params[:id]
    end

    def prepare_action
      @category_blog_infos = CategoryBlogInfo.latest
    end

    def prepare_quick_count(resource)
      resource = BlogInfo.find(resource)
      resource.view_count = resource.view_count+1
      resource.save
    end

end
