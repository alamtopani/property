class Userpage::ApplicationController < Backend::ResourcesController
	layout 'users'

	before_filter :authenticate_user!
  before_filter :prepare_count!
	protect_from_forgery with: :exception

	def authenticate_user!
  	unless current_user.present? && (current_user.member?) || current_user.present? && (current_user.agent?)
      redirect_to root_path, alert: "Can't Access this page"
    end
  end

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, alert: exception.message
  end

  protected
    def prepare_count!
      @properties_size = current_user.properties.size
      @wishlists_size = current_user.wishlists.size
      @inquiries_size = Inquiry.my_messages(current_user.id).roots.size
      @reports_size = current_user.sold_reports.size
    end

    def per_page
      params[:per_page] ||= 20
    end

    def page
      params[:page] ||= 1
    end
end
