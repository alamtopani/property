class Userpage::MembersController < Userpage::ApplicationController
	defaults resource_class: Member, collection_name: 'members', instance_name: 'member'
  load_and_authorize_resource
  
	add_breadcrumb "Dashboard", :userpage_dashboard_path
  add_breadcrumb "Users"

  def update
  	update! do |format|
  		if resource.errors.empty?
        format.html {redirect_to :back}
      else
        format.html {redirect_to :back, errors: resource.errors.full_messages}
      end
  	end
  end

  def upgrade
    resource = User.find params[:id]
    if resource.agent_verify?
      redirect_to userpage_dashboard_path, error: 'Already Verify Become Agent'
    elsif resource.agent_not_verify?
      redirect_to userpage_dashboard_path, notice: 'Your member account already in the process of verification agent of our team, please wait'
    elsif resource.member?
      resource.type = 'Agent'
      resource.verified = false
      if resource.save
        UserMailer.send_request_user_to_agent(resource).deliver
        redirect_to userpage_dashboard_path, notice: 'Your member account today will be in the process of verification of our team, we will tell if this process has been completed!'
      else
        redirect_to userpage_dashboard_path, error: 'Error to be processed!'
      end
    end
  end
end
