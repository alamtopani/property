class Userpage::OrderPackagesController < Userpage::ApplicationController  
  add_breadcrumb "Dashboard", :userpage_dashboard_path
  add_breadcrumb "Invoice", :collection_path

  def index
    @order_packages = collection.where(user_id: current_user.id).latest
    @collection = @order_packages.page(page).per(per_page)
  end

  def show
    add_breadcrumb "Show"
  end
end
