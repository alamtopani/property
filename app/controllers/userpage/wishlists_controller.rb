class Userpage::WishlistsController < Userpage::ApplicationController
  load_and_authorize_resource
  
  add_breadcrumb "Dashboard", :userpage_dashboard_path
  add_breadcrumb "Wishlist", :collection_path

  def index
  	@wishlists = current_user.wishlists.latest
    @collection = @wishlists.page(page).per(per_page)
  end

end
