class Userpage::PropertiesController < Userpage::ApplicationController
  include ActionView::Helpers::NumberHelper
  load_and_authorize_resource

	defaults resource_class: Property, collection_name: 'properties', instance_name: 'property'

	add_breadcrumb "Dashboard", :userpage_dashboard_path
  add_breadcrumb "Properties", :collection_path

  before_filter :prepare_map_location, except: [:index, :destroy]
  before_filter :prepare_map_types

	def index
		@properties = current_user.properties.latest
		@collection = @properties.page(page).per(per_page)
	end

	def edit
    @packages = Package.actived.oldest
    @property = Property.find(params[:id])
    @facilities = AssetFacility.latest.where("LOWER(category) LIKE LOWER('%#{@property.type_property}%')")
		render layout: 'application'
	end

	def create
		build_resource
		resource.user_id = current_user.id
		resource.facilities = params[:facilities]
		create! do |format|
      if resource.errors.empty?
        format.html {redirect_to collection_path}
      else
        format.html {redirect_to :back, error: resource.errors.full_messages}
      end
    end
	end

	def update
		resource.facilities = params[:facilities]
		update! do |format|
      if resource.errors.empty?
        format.html {redirect_to collection_path}
      else
        format.html {redirect_to :back, error: resource.errors.full_messages}
      end
    end
	end

	def renew
		@property = Property.find(params[:id])
    if current_user.user_packages.payed.not_expired.present?
  		@property.update(started_at: Date.today, expired_at: Date.today + 30.days)
  		if @property.save
  			redirect_to :back, notice: "Your Property #{@property.code} Successfully Renew"
  		else
        redirect_to :back, error: 'Error to Renew'
      end
    else
  			redirect_to :back, notice: 'Your Package Is Expired, Please Buy Another Package'
    end
	end

	def do_report_sold
		@report = SoldReport.new(permitted_params_report)
		@report.user_id = current_user.id
		if @report.save
			@property = Property.find(@report.sold_reportable_id)
			@property.update(sold_out: true)
			@property.save
			redirect_to :back, notice: "Sales Report Your Property Already Created"
		else
			redirect_to :back, error: 'Error to Created'
		end
	end

	protected
		def collection
			@collection ||= end_of_association_chain
		end

		def prepare_map_location
			if params[:action] == "edit" || params[:action] == "update"
         village_id = resource.address.village_id
         @others = Property.joins(:address).where("properties.id != ?", resource.id).filter_by_village_id(village_id).check_latlang.map{|l| [l.title, l.address.latitude, l.address.longitude, "#{number_to_currency_br(l.the_price?)} #{l.the_price_status?}", l.type_property, l.category, l.first_image(:small), l.detail_property, number_to_currency_br(l.price_for_meter)]}
       else
         @others = []
       end
		end

    def check_valid_user(property_user, current_user)
      if property_user != current_user
        flash[:errors] = ['You Are Denied To Access This Page']
        redirect_to userpage_dashboard_path
      else
        render layout: 'application'
      end
    end

    def number_to_currency_br(number)
      number = 0 if number.blank?
      number_to_currency(number, :unit => "Rp ", :separator => ",", :delimiter => ".", precision: 0)
    end

		def prepare_map_types
			@types   = PropertyType.roots.without_auction.pluck(:name)
		end

		def permitted_params_report
			params.require(:sold_report).permit(Permitable.controller('userpage/sold_reports'))
		end
end
