class InquiriesController < FrontendController

	def create
		@inquiry = Inquiry.new(permitted_params)

		if current_user
			@inquiry.user_id = current_user.id
		end

		respond_to do |format|
      if @inquiry.save
        format.html {redirect_to :back, notice: 'Pesan Anda Berhasil di Kirim'}
      else
        format.html {redirect_to :back, errors: @inquiry.errors.full_messages}
      end
    end
	end

	private

		def permitted_params
			params.require(:inquiry).permit(Permitable.controller(params[:controller]))
		end
end
