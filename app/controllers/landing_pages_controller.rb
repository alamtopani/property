class LandingPagesController < FrontendController

  def show
    @page = LandingPage.find(params[:id])
    @regions = Region.roots.alfa
  end

  def region
    @region = Region.find_by(name: params[:id])
  end

end
