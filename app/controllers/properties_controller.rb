class PropertiesController < FrontendController
  impressionist actions: [:show]
	before_filter :prepare_map_types, only: [:index, :show]
	before_filter :prepare_map_location, except: [:index, :destroy]
  before_filter :prepare_facilities_new, only: [:new]

	def index
    search = params[:region_display_name].split(',').first if params[:region_display_name]

    reg = search.present? ? search : params[:province_string].present? ? params[:province_string] : params[:province].present? ? params[:province] : "Kota Sukabumi"
    # params[:province_string] = params[:province_string].present? ? params[:province_string] : "Kota Sukabumi" if params[:region_display_name].blank?

    # @featureds = Property.search_by(params).activated.latest.featured.limit(16)
    @region = Region.find_by(name: search) if search.present?

    if params[:search].present?
      prepare_facilities_search(params[:type_property]) if params[:type_property].present?
    end

    # @top_properties = Property.activated.search_by(params).top_listing

    if params[:list].present?
      @region = Region.where(name: reg).first
      @list_properties = Property.search_by(params).activated.latest
      @properties_count = @list_properties.size
      render :map
    else
      if params[:category].blank?
        @properties = collection
      elsif params[:category] == 'Dilelang'
        @properties = collection.activated_auction
      else
        @properties = collection
      end

      @properties_count = @properties.size

			@properties = @properties.page(params[:page]).per(24)
      respond_to do |format|
  			format.html { render :list }
        format.js { render 'index.js'}
      end
		end
	end

  def featured
    @featureds = Property.search_by(params).activated.latest.featured.package_approved.page(params[:page]).per(params[:per_page])
  end

  def auction
    @properties = collection.activated_auction.limit(10)
  end

	def new
		@property = Property.new
    @packages = Package.actived.oldest
	end

	def create
		@property = Property.new(permitted_params)
		@property.user_id = current_user.id
    @property.facilities = params[:facilities]
    check_package_user(current_user)

    if current_user.member? && @ads >= 10
      redirect_to :back, alert: 'Gagal, Mohon maaf limit iklan untuk member biasa sudah melewati batas limit sudah 10 iklan dibuat, Silahkan upgrade ke akun Agen Premium untuk menambah batas limit iklan, Masuk ke menu Daftar Paket untuk melihat paket yang tersedia!'
    elsif current_user.agent_verify? && @ads >= 100
      redirect_to :back, alert: 'Gagal, Mohon maaf limit iklan untuk anda sudah melewati batas limit sudah 100 iklan dibuat!'  
    elsif current_user.expired? && current_user.agent_verify?
      redirect_to :back, alert: 'Gagal, Mohon maaf Akun agen premium anda sudah melewati masa aktif, silahkan perbaharui dengan membayar biaya perpanjangan paket ke no rekening yang tersedia di rumahdaku.com, Lalu konfirmasi pembayaran paket anda di halaman konfirmasi, Petugas kami akan memverifikasi pembayaran anda. Proses verifikasi max 1x24 jam. Terimakasih!'     
    else
      if @property.save
        UserMailer.send_request_catalog(@property).deliver
        redirect_to userpage_properties_path, notice: 'Iklan anda berhasil dibuat dan akan segera bisa dilihat setelah di verifikasi oleh Staf Admin Kami, Verifikasi Max 1 x 24 Jam! Terimakasih!'
      else
        flash[:errors] = @property.errors.full_messages
        redirect_to :back
      end
    end
	end

	def show
    resource
    prepare_quick_count(resource)

		@property = Property.find(params[:id])
		@inquiry = Inquiry.new
    @facilities = AssetFacility.latest.where("LOWER(category) LIKE LOWER('%#{@property.type_property}%')")
    rating(@property.id)
    impressionist(@property, 'message')

    @other_properties = Property.where.not("properties.id =?", @property.id).activated.package_approved.latest.limit(3)
	end

  def delete_reviews
    rate = Rate.find(params[:rate_id])
    if rate.delete
      flash[:success] = 'Review anda berhasil di hapus!'
    else
      flash[:error] = rate.errors.full_message
    end
    redirect_to property_path(params[:id])
  end

	def wishlist
    checking = Wishlist.where(property_id: params[:id], user_id: current_user.id)

    if checking.present?
      redirect_to :back, alert: "Iklan ini sudah disimpan di iklan favorit anda!"
    else
      @wishlist             = Wishlist.new
      @wishlist.user_id     = current_user.id
      @wishlist.property_id = params[:id]

      if @wishlist.save
        redirect_to :back, notice: "Iklan ini berhasil di simpan di iklan favorit anda"
      else
        redirect_to :back, alert: "Iklan ini gagal di simpan!"
      end
    end
  end

  def checkout_bid
    @bid_auction = BidAuction.new
    @bid_auction.user_id = current_user.id
    @bid_auction.property_id = params[:property_id]
    @bid_auction.bid = params[:bid]

    @property = Property.find params[:property_id]
    if params[:bid].to_i < @property.the_price_bid?.to_i
      redirect_to :back, alert: "Penawaran anda haruslah lebih besar dibandingkan harga properti atau lebih besar dari penawaran masuk tertinggi!"
    else
      if @bid_auction.save
        redirect_to :back, notice: "Penawaran anda berhasil dikirimkan"
      else
        redirect_to :back, alert: "Penawaran anda gagal dikirim!"
      end
    end
  end

	private
    def resource
      @property = Property.find params[:id]
    end

    def collection
      @collection ||= Property.search_by(params).activated.latest.package_approved
    end

		def prepare_map_types
      @types   = PropertyType.roots.without_auction.pluck(:name)
      @types_location = LocationType.to_a
      @facilities = AssetFacility.pluck(:name)
      @assets = []
    end

    def prepare_map_location
      @types   = PropertyType.roots.without_auction.pluck(:name)
      @others = []
		end

		def permitted_params
			params.require(:property).permit(Permitable.controller(params[:controller]))
		end

    def prepare_facilities_new
      @facilities = AssetFacility.latest
    end

    def prepare_facilities_search(type)
      @facility_list = AssetFacility.latest.where("LOWER(category) LIKE LOWER('%#{type}%')")
    end

    def rating(property_id)
      @reviews = Rate.where(rateable_id: property_id).page(page).per(per_page)
    end

    def prepare_quick_count(resource)
      resource = Property.find(resource)
      resource.view_count = resource.view_count+1
      resource.save
    end

    def check_package_user(user)
      @ads = user.properties.size
    end
end
