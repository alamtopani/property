class ImportWorker
  include Sidekiq::Worker
  sidekiq_options retry: false, queue: :export, backtrace: true

  sidekiq_retries_exhausted do |msg|
    Sidekiq.logger.warn "Failed #{msg['class']} with #{msg['args']}: #{msg['error_message']}"
  end

  def perform(type,args={})
    ImportWorker.start(type, args)
  end

  class << self
    def perform_by_env(type, args={})
      if Rails.env.development?
        start(type, args)
      else
        perform_async(type, args)
      end
    end

    def start(type, args={})
      exporter = type.constantize rescue nil
      if exporter
        new_export = exporter.new(args.deep_symbolize_keys)
        new_export.start
        puts type
        puts args
        puts new_export.errors.join(', ')
      end
    end
  end
end
