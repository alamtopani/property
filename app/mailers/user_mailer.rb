class UserMailer < ActionMailer::Base
  include ActionView::Helpers::NumberHelper
  helper :application

	default from: 'info.rumahdaku@gmail.com'
	default to: 'info.rumahdaku@gmail.com'

	# USER MAILER

	def welcome_join(user)
		#@logo = "#{root_url}assets/logo.png"
		@url = "#{root_url}"
		@user = user
		mail(to: user.email, subject: 'Selamat Datang di RumahDaku')
	end

	def send_request_user_to_agent(user)
		#@logo = "#{root_url}assets/logo.png"
		@url = "#{root_url}"
		@user = user
		mail(from: user.email, subject: 'Permohonan Verifikasi - Perubahan Akun Biasa Ke Agen')
	end

	def send_user_approve_to_agent(user)
		#@logo = "#{root_url}assets/logo.png"
		@url = "#{root_url}"
		@user = user
		mail(to: user.email, subject: 'Persetujuan Verifikasi - Akun Anda Berhasil di Rubah Ke Akun Agen')
	end

	def send_user_approve_to_member(user)
		#@logo = "#{root_url}assets/logo.png"
		@url = "#{root_url}"
		@user = user
		mail(to: user.email, subject: 'Persetujuan Verifikasi  - Akun Member Anda Sudah di Verifikasi')
	end

	def send_user_approve_to_verified(user)
    @user = user
    mail(to: user.email, subject: 'Persetujuan Verifikasi - Akun anda berhasil dirubah ke akun Agen Premium')
  end

  def send_user_approve_to_verified_extension(user)
    @user = user
    mail(to: user.email, subject: 'Persetujuan Perpanjangan Akun Agen Premium - Akun anda berhasil diperpanjang')
  end

	# FEEDBACK MAILER

	def send_feedback(feedback)
		#@logo = "#{root_url}assets/logo.png"
		@feedback = feedback
		mail(from: feedback.email, subject: "Permohonan Feedback - #{feedback.code}")
	end

	# REPORT MAILER

	def send_report(report)
		#@logo = "#{root_url}assets/logo.png"
		@report = report
		mail(from: report.email, subject: "Di Terima Report - #{report.code}")
	end

	# PROPERTY MAILER

	def send_request_catalog(catalog)
		#@logo = "#{root_url}assets/logo.png"
		@catalog = catalog
		mail(from: catalog.user.email, subject: "Permohonon Persetujuan Verifikasi Iklan - #{catalog.code}")
	end

	def send_catalog_verified(catalog)
		#@logo = "#{root_url}assets/logo.png"
		@catalog = catalog
		mail(to: catalog.user.email, subject: "Property Berhasil di Publish - #{catalog.code}")
	end

	def send_catalog_featured(catalog)
		#@logo = "#{root_url}assets/logo.png"
		@catalog = catalog
		mail(to: catalog.user.email, subject: "Perubahan Properti Menjadi Properti Yang Jadi Pilihan  - #{catalog.code}")
	end

	def add_order_package(order)
    @order = order
    mail(from: @order.user.email, subject: "Daftar Paket Premium - #{@order.code}")
  end

  def add_order_package_invoice(order)
    @order = order
    mail(to: @order.user.email, subject: "Invoice Daftar Paket Premium - #{@order.code}")
  end

  def send_confirmation(confirmation)
    @confirmation = confirmation
    mail(from: @confirmation.email, subject: "Konfirmasi order paket - #{@confirmation.code}")
  end

	# def send_same_property(catalogs, user, village_name)
	# 	@user = user
	# 	@catalogs = catalogs
	# 	@village_name = village_name
	# 	mail(to: @user.email, subject: "New Property Same With Your Wishlist")
	# end

end
