class Interior < ActiveRecord::Base
	belongs_to :interiorable, polymorphic: true
	include ScopeBased
	default_scope { order('interiors.position ASC') }
end
