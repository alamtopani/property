class FeedbackCategory < EnumerateIt::Base
  associate_values :Report, :Feedback
end
