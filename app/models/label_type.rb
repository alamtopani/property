class LabelType < EnumerateIt::Base
  associate_values(
    :hot_deals              => ['Hot Deals', 'Hot Deals'],
    :fair_deals             => ['Fair Deals', 'Fair Deals'],
    :high_price_deals       => ['High Price Deals', 'High Price Deals'],
    :good_deals             => ['Good Deals', 'Good Deals'],
  )
  sort_by :value
end
