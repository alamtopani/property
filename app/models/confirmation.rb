class Confirmation < ActiveRecord::Base
  before_create :prepare_code
  extend FriendlyId
  friendly_id :code, use: [:slugged, :finders]

  belongs_to :user, foreign_key: 'user_id'

  include ScopeBased
  include TheOrder::ConfirmationSearching

  private
    def prepare_code
      self.code = SecureRandom.hex(6) if self.code.blank?
    end
end
