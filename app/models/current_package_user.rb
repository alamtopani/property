class CurrentPackageUser < ActiveRecord::Base
  belongs_to :user

  scope :expired, ->{ where("expire_in < ?", Date.today) }
  scope :not_expired, ->{ where("expire_in > ? OR expire_in IS NULL", Date.today) }
end
