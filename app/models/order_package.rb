class OrderPackage < ActiveRecord::Base
  include ScopeBased
  before_create :prepare_code

  belongs_to :user, foreign_key: 'user_id'
  belongs_to :staf, class_name: 'User', foreign_key: 'staf_id'

  include TheOrder::OrderSearching
  scope :bonds, -> { eager_load(:user) }

  def status?
    if self.user.agent_verify? && self.user.expired_at > Date.today && self.status == true
      return "<span class='btn btn-success btn-xs'>Paid</span>".html_safe
    elsif self.user.agent_verify? && self.user.expired_at < Date.today && self.status == true
      return "<span class='btn btn-warning btn-xs'>Expired</span>".html_safe
    else
      return "<span class='btn btn-danger btn-xs'>Pending</span>".html_safe
    end
  end

  def expired?
    self.user.expired_at < Date.today && self.status == true || self.status == false
  end

  def act_expired?
    self.user.expired_at < Date.today && self.status == true
  end

  def act_new_order?
    self.status == false
  end

  private
    def prepare_code
      self.code = SecureRandom.hex(3) if self.code.blank?
    end
end
