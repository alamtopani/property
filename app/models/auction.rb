class Auction < ActiveRecord::Base
	belongs_to :auction_property, foreign_key: 'property_id'
	include ScopeBased
end
