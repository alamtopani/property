class Testimonial < ActiveRecord::Base
  include ScopeBased
  scope :activated, ->{ where(activated: true) }

  belongs_to :user, foreign_key: 'user_id'

end
