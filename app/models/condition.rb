class Condition < EnumerateIt::Base
  associate_values :yes, :no
end
