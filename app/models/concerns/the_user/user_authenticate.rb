module TheUser
  module UserAuthenticate
  	extend ActiveSupport::Concern
    included do
      attr_accessor :login

    	def authenticate(login, password)
        user = where("username = :login OR email = :login", { login: login }).first
        return nil unless user
        return nil unless user.valid_password?(password)
        user
      end

      def self.find_for_database_authentication(warden_conditions)
        conditions = warden_conditions.dup
        if login = conditions.delete(:login)
          where(conditions).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
        else
          where(conditions).first
        end
      end

      def login=(login)
        @login = login
      end

      def login
        @login || self.username || self.email
      end

      def self.find_for_facebook_oauth(auth)
        user = User.where(:provider => auth.provider, :uid => auth.uid).first
        if user.present?
          return user
        else
          registered_user = User.where(:email => auth.info.email).first
          if registered_user.present?
            return registered_user
          else
            user = User.new(username: auth.extra.raw_info.name, email: auth.info.email, provider: auth.provider, uid: auth.uid, type: 'Member', confirmation_token: nil, confirmed_at: Time.now, password: Devise.friendly_token[0,10])
          end
        end
      end

    end
  end
end
