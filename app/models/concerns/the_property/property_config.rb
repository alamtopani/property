module TheProperty
	module PropertyConfig
		extend ActiveSupport::Concern

		included do
			is_impressionable
			after_initialize :after_initialized, :populate_galleries

			# validate :check_validity, on: :create
			before_save :prepare_price_in_meter, :prepare_starting_bid
			before_create :prepare_code #:set_type_package

			belongs_to :user, foreign_key: 'user_id'

			has_one :address, as: :addressable, dependent: :destroy
			accepts_nested_attributes_for :address, reject_if: :all_blank, allow_destroy: true

		  has_one :basic_property, foreign_key: 'property_id', dependent: :destroy
		  accepts_nested_attributes_for :basic_property, reject_if: :all_blank, allow_destroy: true

			has_one :auction, foreign_key: 'property_id', dependent: :destroy
			accepts_nested_attributes_for :auction, reject_if: :all_blank, allow_destroy: true

			has_many :galleries, as: :galleriable, dependent: :destroy
			accepts_nested_attributes_for :galleries, reject_if: :all_blank, allow_destroy: true

			has_many :wishlists, foreign_key: 'property_id', dependent: :destroy
			accepts_nested_attributes_for :wishlists, reject_if: :all_blank, allow_destroy: true

			has_many :schedule_visits, foreign_key: 'property_id', dependent: :destroy
			accepts_nested_attributes_for :schedule_visits, reject_if: :all_blank, allow_destroy: true

			has_one :inquiries, as: :inquiriable

			has_many :reports, as: :reportable, dependent: :destroy
			accepts_nested_attributes_for :reports, reject_if: :all_blank, allow_destroy: true

			has_one :sold_report, as: :sold_reportable, dependent: :destroy
			accepts_nested_attributes_for :sold_report, reject_if: :all_blank, allow_destroy: true

			has_many :bid_auctions, foreign_key: 'property_id', dependent: :destroy
			accepts_nested_attributes_for :bid_auctions, reject_if: :all_blank, allow_destroy: true

			scope :featured, ->{where(featured: true)}
			scope :filter_by_city, ->(city) {eager_load(:basic_property, :galleries, :address).joins(:address).where("LOWER(addresses.city) = LOWER(?)", city)}
			scope :filter_by_type, ->(type) {where("LOWER(properties.type_property) = LOWER(?)", type) if type}
			scope :filter_by_village_id, ->(village_id) {joins(:address).where("addresses.village_id = ?", village_id)}
			scope :check_latlang, ->{ where.not("addresses.longitude =? and addresses.longitude =?", 'nil', 'nil') }
			scope :activated, ->{ where(activated: true).where.not(sold_out: true) }
			scope :expired, ->{ where(activated: true)}
			scope :non_activated, ->{ where(activated: false) }
			scope :not_auction, ->{where.not(category: 'Dilelang')}
			scope :yes_auction, ->{where(category: 'Dilelang')}
			scope :activated_auction, ->{ where("auctions.end_auction >=?", Date.today) }
			scope :popular_property, -> {order(view_count: :desc)}
			scope :package_approved, ->{bonds.where("users.package IN (?) OR users.expired_at >?", 'free', Date.today)}
			scope :bonds, ->{
												eager_load(
																		{user: [:profile]},
																		:address,
																		:basic_property,
																		:auction,
																		:galleries
																	)
											}
		end

		def the_rent?
			self.category == 'Disewa'
		end

		def the_sell?
			self.category == 'Dijual'
		end

		def the_pawning?
			self.category == 'Digadai'
		end

		def the_auction?
			self.category == 'Dilelang'
		end

		def latitude
	    address.latitude
	  end

	  def longitude
	    address.longitude
	  end

	  def price_per_meter
	  	basic_property.price_in_meter
	  end

	  def first_image(style=:thumb)
			self.try(:galleries).try(:latest_position).try(:first).try(:file).url(style)
	  end

		def build_image(media_title, position=nil)
	    gallery = self.galleries.build({title: media_title})
	    gallery.position = position
	  end

	  def map_exist?
	  	self.address.latitude.present? && self.address.longitude.present?
	  end

	  def active?
	  	if self.activated == true
	  		return "<i class='fa fa-check'></i> Sudah di Tayangkan".html_safe
	  	else
	  		return "<i class='fa fa-exclamation'></i> Belum di Tayangkan".html_safe
	  	end
	  end

	  def featured?
	  	if self.featured == true
	  		return "[Featured]".html_safe
	  	end
	  end

	  def sold?
	  	self.sold_out == true
	  end

	  def sold_label?
	  	if self.sold?
	  		return "<label class='label label-danger'>Terjual</label>".html_safe
	  	end
	  end

	  def the_price?
	  	if self.the_rent?
	  		if self.type_property == 'Tanah' || self.basic_property.price_month.to_i < 1
	  			return "#{self.basic_property.price_year}" rescue 0
	  		else
	  			return "#{self.basic_property.price_month}" rescue 0
	  		end
	  	elsif self.is_category_auction?
  			return "#{self.auction.starting_bid}" rescue 0
	  	else
	  		return "#{self.price}" rescue 0
	  	end
	  end

	  def the_price_status?
	  	if self.the_rent?
	  		if self.type_property == 'Tanah' || self.basic_property.price_month.to_i < 1
	  			return "<small class='color-grey'> Per tahun</small>".html_safe rescue "-"
	  		else
	  			return "<small class='color-grey'> Per bulan</small>".html_safe rescue "-"
	  		end
	  	elsif self.is_category_auction?
	  		return "<small class='color-grey'> Harga Mulai Dari</small>".html_safe rescue "-"
	  	elsif self.the_pawning?
	  		return "<small class='color-grey'> Harga Gade</small>".html_safe rescue "_"
	  	else
	  		return "<small class='color-grey'> Harga Properti</small>".html_safe rescue "_"
	  	end
	  end

	  def the_price_bid?
	  	if self.bid_auctions.size > 0
	  		return "#{self.bid_auctions.top_rank.first.bid}"
	  	else
	  		return "#{self.auction.starting_bid}"
	  	end
	  end

	  def bid_status?
	  	if self.bid_auctions.size > 0
	  		return 'Penawaran Tertinggi'
	  	else
	  		return 'Harga Dimulai Dari'
	  	end
	  end

	  def set_expired_time?
	  	if self.started_at == nil && self.expired_at == nil
	  		self.started_at = Date.today
				self.expired_at = Date.today + 30.days
	  	end
	  end

	  def is_expired?
	  	# self.expired_at < Date.today rescue nil
	  end

	  def is_category_auction?
	  	self.category == 'Dilelang'
	  end

		private

			def prepare_code
				self.code = SecureRandom.hex(3) if self.code.blank?
			end

			def set_type_package
				self.type_package = self.user.type_package
			end

			def prepare_price_in_meter
				if self.the_sell? || self.the_pawning?
					if self.type_property == 'Apartemen' || self.type_property == 'Perkantoran'
						self.basic_property.price_in_meter = self.price/self.basic_property.building_area.to_i if self.basic_property.building_area.present?
					elsif self.type_property == 'Tanah' || self.type_property == 'Rumah'
						self.basic_property.price_in_meter = self.price/self.basic_property.surface_area.to_i if self.basic_property.surface_area.present?
					else
						self.basic_property.price_in_meter = nil
					end
				end
			end

			def after_initialized
				self.address ||= Address.new if self.new_record?
	      self.basic_property ||= BasicProperty.new if self.new_record?
	      self.auction ||= Auction.new if self.new_record?
			end

			def populate_galleries
	      if self.galleries.length < 6
	        [
	          'Cover',
	          'Image1',
	          'Image2',
	          'Image3',
	          'Image4',
	          'Image5',
	        ].each_with_index do |media_title, index|
	          _galery = self.galleries.select{|g| g.title.to_s.downcase == media_title.downcase}.first
	          unless _galery
	            self.build_image(media_title, index+1)
	          else
	            _galery.position = index+1
	          end
	        end
	      end if self.new_record?
	    end

	    def prepare_starting_bid
	      if self.auction.minimum_price.present?
	        start_bid = self.auction.minimum_price.to_i * 20/100
	        self.auction.starting_bid = self.auction.minimum_price.to_i - start_bid
	      end
	    end

	    # def check_validity
	    # 	user = self.user
	    # 	user_package = user.current_package
	    # 	user_properties = user.properties
	    # 	if user_package.present?
	    # 		if user_package.expire_in.try('>', Date.today) || user_package.expire_in.nil?
	    # 			if (user_package.max_listing - 1) >= user_properties.count
	    # 				if self.featured
	    # 					if (user_package.featured_listing - 1) >= user_properties.featured.count
	    # 						true
	    # 					else
					#     		errors.add(:user, "Your Listing Featured Is Maximaze, Please Buy Another Apackage")
					#     		false
		   #  				end
	    # 				else
	    # 					if (user_package.featured_listing - 1) >= user_properties.featured.count
	    # 						self.featured = true
	    # 						true
	    # 					else
					#     		self.featured = false
					#     		true
		   #  				end
	    # 				end
	    # 			else
			  #   		errors.add(:user, "Your Listing Is Maximaze, Please Buy Another Package")
			  #   		false
	    # 			end
	    # 		else
		   #  		errors.add(:user, "You Don't Have Active Packages, Please Buy Any Package")
		   #  		false
	    # 		end
	    # 	else
	    # 		errors.add(:user, "You Don't Have Packages, Please Buy Any Package")
	    # 		false
	    # 	end
	    # end

	    def send_email_same_property
	      users = User.get_users_same_with_catalog_and_wishilist(self)
	    	return if users.blank?
	      users.each do |user|
	        UserMailer.send_same_property(self, user).deliver
	      end
	    end

	end
end
