module TheOrder
  module OrderSearching
    extend ActiveSupport::Concern

    module ClassMethods

      def by_keywords(_key)
        return if _key.blank?
        query_opts = [
          "LOWER(order_packages.code) LIKE LOWER(:key)",
          "LOWER(users.username) LIKE LOWER(:key)",
          "LOWER(users.email) LIKE LOWER(:key)"
        ].join(' OR ')
        where(query_opts, {key: "%#{_key}%"} )
      end

      def by_status(_status)
        return if _status.blank?
        if _status == 'expired'
          where("users.expired_at <?", Date.today)
        else
          where("order_packages.status =?", _status)
        end
      end

      def by_start_at(_start_at)
        return if _start_at.blank?
        where("order_packages.updated_at >=?", _start_at.to_date)
      end

      def by_end_at(_end_at)
        return if _end_at.blank?
        where("order_packages.updated_at <=?", _end_at.to_date)
      end


      def search_by(options={})
        results = bonds

        if options[:key].present?
          results = results.by_keywords(options[:key])
        end

        if options[:status].present?
          results = results.by_status(options[:status])
        end

        if options[:start_at].present?
          results = results.by_start_at(options[:start_at])
        end

        if options[:end_at].present?
          results = results.by_end_at(options[:end_at])
        end

        return results
      end

    end
  end
end
