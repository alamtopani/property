module TheLocation
	module LocationSearching
		extend ActiveSupport::Concern

		module ClassMethods
			def by_keywords(_key)
				return if _key.blank?
				query_opts = [
					"LOWER(locations.title) LIKE LOWER(:key)",
					"LOWER(locations.type_asset) LIKE LOWER(:key)",
					"LOWER(users.username) LIKE LOWER(:key)",
					"LOWER(users.email) LIKE LOWER(:key)"
				].join(' OR ')
				where(query_opts, {key: "%#{_key}%"} )
			end

			def by_type_asset(_type_asset)
				return if _type_asset.blank?
				where("locations.type_asset =?", _type_asset)
			end

			def by_province(_province)
				return if _province.blank?
				where("addresses.province =?", _province)
			end

			def by_city(_city)
				return if _city.blank?
				where("addresses.city =?", _city)
			end

			def search_by(options={})
				results = bonds

				if options[:key].present?
					results = results.by_keywords(options[:key])
				end

				if options[:type_asset].present?
					results = results.by_type_asset(options[:type_asset])
				end

				if options[:province].present?
					results = results.by_province(options[:province])
				end

				if options[:city].present?
					results = results.by_city(options[:city])
				end

				return results
			end
		end

	end
end
