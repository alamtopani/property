class Package < ActiveRecord::Base
  include ScopeBased
  has_many :user_packages
  has_many :users, through: :user_packages
  scope :actived, ->{ where(active: true) }

  def status?
    if self.active == true
      return "<i class='fa fa-check'></i> Activated".html_safe
    else
      return "<i class='fa fa-warning'></i> Not Activated".html_safe
    end
  end
end
