class Location < ActiveRecord::Base
	after_initialize :after_initialized

	include ScopeBased
	include TheLocation::LocationSearching

	scope :alfa, ->{order(title: :asc)}

	has_one :address, as: :addressable, dependent: :destroy
	accepts_nested_attributes_for :address, reject_if: :all_blank, allow_destroy: true

	belongs_to :user, foreign_key: 'user_id'

  scope :filter_by_city, ->(city){select("locations.id, locations.title, locations.latitude, locations.longitude, locations.type_asset")
                                  .joins(:address).where("LOWER(addresses.city) = LOWER(?)", city)}
  scope :filter_by_village_id, ->(village_id){select("locations.id, locations.title, locations.latitude, locations.longitude, locations.type_asset")
                                  .joins(:address).where("addresses.village_id = ?", village_id)}
  scope :filter_by_type, ->(type){where("LOWER(locations.type_asset) = LOWER(?)", type) if type.present? }

	scope :bonds, ->{
										eager_load(:user, :address)
									}

	def map_exist?
  	self.latitude.present? && self.longitude.present?
  end

  def verified?
  	if self.verified == true
  		return "<i class='fa fa-check'></i> Terverifikasi".html_safe
	  else
	  	return "<i class='fa fa-warning'></i> Tidak Terverifikasi".html_safe
	  end
  end

  def self.filter_kilos_by_city(property_id, city, limit=5)
    data = joins(:address).where("LOWER(addresses.city) = LOWER(?)", city)
    hash = data.group_by{ |p| p.type_asset}
    location_ids = hash.map{ |k, v| v.map(&:id).take(limit) }
    AssetProperty.joins(:location).where(location_id: location_ids, property_id: property_id)
  end

  def self.filter_by_city_not_present_in_kilos(property_id, city, limit=5)
    location_ids = AssetProperty.where(property_id: property_id).pluck(:location_id)
    location_ids = [0] if location_ids.blank?
    data = select("locations.id, locations.title, locations.latitude, locations.longitude, locations.type_asset").joins(:address).where("LOWER(addresses.city) = LOWER(?)", city).where("locations.id NOT IN (?)", location_ids)
    hash = data.group_by{ |p| p.type_asset}
    result = hash.map{ |k, v| v.take(limit) }
  end

  def self.filter_by_city_limit_by_group(property_id, city, limit=5)
    data = select("locations.id, locations.title, locations.latitude, locations.longitude, locations.type_asset").joins(:address).where("LOWER(addresses.city) = LOWER(?)", city)
    hash = data.group_by{ |p| p.type_asset}
    result = hash.map{ |k, v| v.take(limit) }
  end

  def self.filter_by_village_id_limit_by_group(property_id, village_id, limit=5)
    data = select("locations.id, locations.title, locations.latitude, locations.longitude, locations.type_asset").joins(:address).where("addresses.village_id = ?", village_id)
           .where("locations.type_asset != 'Airports'")
    village = Region.find(village_id)
    city_id = village.parent.parent.id
    data_airports = select("locations.id, locations.title, locations.latitude, locations.longitude, locations.type_asset")
                    .joins(:address).where("addresses.city_id = ?", city_id).where("locations.type_asset = 'Airports'")
    data = data + data_airports
    hash = data.group_by{ |p| p.type_asset}
    result = hash.map{ |k, v| v.take(limit) }
  end

  def self.filter_by_subdistrict_id_limit_by_group(property_id, subdistrict_id, limit=5)
    data = select("locations.id, locations.title, locations.latitude, locations.longitude, locations.type_asset").joins(:address).where("addresses.subdistrict_id = ?", subdistrict_id)
    hash = data.group_by{ |p| p.type_asset}
    result = hash.map{ |k, v| v.take(limit) }
  end

	private
		def after_initialized
			self.address ||= Address.new if self.new_record?
		end
end
