class BlogInfo < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: [:slugged, :finders]

  include ScopeBased
  include TheBlog::BlogScope
  include TheBlog::BlogSearching
  belongs_to :category_blog_info, foreign_key: 'category_id'
  belongs_to :user, foreign_key: 'user_id'

  scope :verified, -> {where(status: true)}
  scope :featured, -> {where(featured: true)}
  scope :headline, -> {order(view_count: :desc)}
  scope :info, -> {where("category_blog_infos.name =?", 'Info')}
  scope :tips, -> {where("category_blog_infos.name =?", 'Tips dan Trik')}
  scope :event, -> {where("category_blog_infos.name =?", 'Event')}
  scope :bonds,-> {
    eager_load(:category_blog_info)
  }

  has_attached_file :cover, styles: {
                      medium:   '500x500>',
                      small:   '250x250>',
                      thumb:    '128x128>',
                    },
                    default_url: 'no-image.png'

  validates_attachment :cover, content_type: {
    content_type: ["image/jpg", "image/jpeg", "image/png"]
  }

  def active?
    if self.status == true
      return "<i class='fa fa-check'></i>Published".html_safe
    else
      return "<i class='fa fa-exclamation'></i>Not Published".html_safe
    end
  end

  def featured?
    if self.featured == true
      return "[ <i class='fa fa-check'></i>Featured ]".html_safe
    else
      return "".html_safe
    end
  end
end
