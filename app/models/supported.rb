class Supported < ActiveRecord::Base
  include ScopeBased

  validates :title, presence: true

  has_attached_file :logo, styles: {
                      large:    '1000>',
                      medium:   '500>',
                      small:    '300>',
                      thumb:    '64x64>'
                    },
                    default_url: 'no-image.png'

  validates_attachment :logo, content_type: {
    content_type: ["image/jpg", "image/jpeg", "image/png"]
  }
end
