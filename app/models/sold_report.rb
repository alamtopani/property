class SoldReport < ActiveRecord::Base
	belongs_to :sold_reportable, polymorphic: true
	belongs_to :user, foreign_key: 'user_id'

	scope :bonds, ->{joins( "INNER JOIN properties ON sold_reports.sold_reportable_id = properties.id" ).where( sold_reports: { sold_reportable_type: 'Property' } )}

	include ScopeBased
  include TheReport::SoldReportSearching
end
