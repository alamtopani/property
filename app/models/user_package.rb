class UserPackage < ActiveRecord::Base
  belongs_to :user
  belongs_to :package
  has_one :current_package_user, through: :user

  scope :payed, ->{ where(payed: true) }
  scope :not_payed, ->{ where(payed: false) }
  scope :expired, ->{ where("expire_in < ?", Date.today) }
  scope :not_expired, ->{ where("expire_in > ? OR expire_in IS NULL", Date.today) }
  scope :top_listing, ->{ where(top_listing: true) }
  scope :not_free, ->{ joins(:package).where('packages.name != ?', "Free Package") }

  before_create :before_save

  include ScopeBased

  validates_uniqueness_of :code, if: 'self.code.present?'

  def payed?
    if self.payed == true
      return "<button class='btn btn-success btn-xs'>Paid</button>".html_safe
    else
      return "<button class='btn btn-danger btn-xs'>Pending</button>".html_safe
    end
  end

  def top_listing?
    if self.top_listing == true
      return "Yes".html_safe
    else
      return "No".html_safe
    end
  end

  def confirmation
    package = self.package
    time_now ||= Time.now
    self.payed = true
    self.expire_in = time_now + package.duration.to_i.months + package.gratis_bulan.to_i.months
    self.confirmation_at = time_now
    self.save
    add_to_current_package
    PackageMailer.confirmation(self).deliver
    true
  end

  def before_save
    package = self.package
    self.max_listing = package.max_listing
    self.featured_listing = package.featured_listing
    self.top_listing = package.top_listing
    self.package_name = package.name
    self.price = package.price
    self.label_tags = package.label_tags
    generate_code
  end

  def generate_code
    chars = ('a'..'z').to_a + ('A'..'Z').to_a
    generate_code = (0...8).collect { chars[Kernel.rand(chars.length)] }.join
    self.code = generate_code
  end

  def add_to_current_package
    package = self.package
    expire = package.duration.to_i + package.gratis_bulan.to_i
    current = CurrentPackageUser.find_or_initialize_by(user_id: self.user_id)
    current.expire_in = process_expire(current.expire_in, expire)
    current.label_tags = process_label_tags(current.label_tags, self.label_tags.split(',')) if self.label_tags.present?
    current.max_listing = current.max_listing.to_i + self.max_listing
    current.featured_listing = current.featured_listing.to_i + self.featured_listing.to_i
    current.top_listing = self.top_listing if self.top_listing == true
    current.save
  end

  def process_label_tags(current_label, new_label)
    return new_label if current_label.nil?
    new_current_label = current_label.split(',') + new_label
    new_current_label.uniq.join(',')
  end

  def process_expire(current_expire, new_expire)
    return nil if new_expire.nil? || new_expire.zero?
    return Date.today + new_expire.months if current_expire.nil? || current_expire < Date.today
    if self.expire_in.nil?
      nil
    else
      current_expire + new_expire.months
    end
  end
end
