class LocationType < EnumerateIt::Base
  associate_values(
    :airports             	=> ['airport', 'Airports'],
    :train_stations         => ['train_station', 'Train Stations'],
    :bus_stations			=> ['bus_station', 'Bus Stations'],
    :bank					=> ['bank', 'Banks'],
    :hospitals				=> ['hospital', 'Hospitals'],
    :schools				=> ['school', 'Schools'],
    :parks					=> ['park', 'Parks'],
    :restaurants			=> ['restaurant', 'Restaurants'],
    :movie_theaters			=> ['movie_theater', 'Movie Theaters'],
    :bars					=> ['bar', 'Bars'],
    :department_stores		=> ['department_store', 'Department Stores'],
    :pharmacies				=> ['pharmacy', 'Pharmacies'],
    :shopping_malls			=> ['shopping_mall', 'Shopping Malls']
  )
  sort_by :value
end
