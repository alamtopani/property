class Visit < ActiveRecord::Base
  has_many :ahoy_events, class_name: "Ahoy::Event"
  belongs_to :user

  def self.count_today
    where("DATE(started_at) = DATE(?)", Date.today).count
  end

  def self.count_week
    today = Date.today
    where("DATE(started_at) >= DATE(?) AND DATE(started_at) <= DATE(?)", today.at_beginning_of_week, today.at_end_of_week).count
  end

  def self.count_month
    today = Date.today
    where("DATE(started_at) >= DATE(?) AND DATE(started_at) <= DATE(?)", today.at_beginning_of_month, today.at_end_of_month).count
  end

  def self.count_year
    today = Date.today
    where("DATE(started_at) >= DATE(?) AND DATE(started_at) <= DATE(?)", today.beginning_of_year, today.end_of_year).count
  end

  def self.per_year
    today = Date.today
    where("DATE(started_at) >= DATE(?) AND DATE(started_at) <= DATE(?)", today.beginning_of_year, today.end_of_year)
  end

  def self.per_month
    today = Date.today
    where("DATE(started_at) >= DATE(?) AND DATE(started_at) <= DATE(?)", today.at_beginning_of_month, today.at_end_of_month)
  end
end
