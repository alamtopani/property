class BasicProperty < ActiveRecord::Base
	belongs_to :property, foreign_key: 'property_id'

  def the_age?
    if self.age.present?
      return Date.today.strftime("%Y").to_i - self.age.to_i
    end
  end
end
