$(document).ready(function() {

  // SELECT2 DISPLAY
  $(".select2").select2({
  	maximumSelectionLength: 2
  });

  // GALLERY ON SHOW
  $('.choice-gallery-form .input-file').on('change', function(index){
    if($(this).length){
      previewFile(this, $(this).parent('li').find('.show-choice'));
    }
  });

  propertyCheck();

  // CATEGORIES ONCHANGE
  $('.categories_select').on('change', function(){
    if($(this).val() != '' && $(this).val() != 'undefined'){
      $.get('/get_categories?name='+$(this).val()+'&node='+$(this).data("node"), function(result){
        $('.category-container').html(result);
        $('.category-container .select2').select2();
      })
      if($('.categories_select:checked').val() == 'Disewa'){
        $('.price-sell').hide();
      }else if($('.categories_select:checked').val() == 'Dijual' || $('.categories_select:checked').val() == 'Digadai'){
        $('.price-sell').show();
      }else if($('.categories_select:checked').val() == 'Dilelang'){
        $('.price-sell').show();
      }
    }
  });

  if($('.categories_select:checked').val() == 'Disewa'){
    RentCategoriesSelected($('.type_properties_select').val());
    $('.price-sell').hide();
  }else if($('.categories_select:checked').val() == 'Dijual' || $('.categories_select:checked').val() == 'Digadai'){
    SellCategoriesSelected($('.type_properties_select').val());
  }else{
    SellCategoriesSelected($('.type_properties_select').val());
  }

  $('.category-container').on('change', '.type_properties_select', function(){
    if($('.categories_select:checked').val() == 'Dijual' || $('.categories_select:checked').val() == 'Digadai'){
      if($(this).val() != '' && $(this).val() != 'undefined'){
        SellCategoriesSelected($(this).val());
      }
    }else if($('.categories_select:checked').val() == 'Disewa'){
      if($(this).val() != '' && $(this).val() != 'undefined'){
        RentCategoriesSelected($(this).val());
      }
    }else{
      if($(this).val() != '' && $(this).val() != 'undefined'){
        SellCategoriesSelected($(this).val());
      }
    }

    CheckFacility($(this).val());
  });

  RegionChangeFormat();

  // $('#property_user_id').on('change', function(){
  //   if($(this).val() != '' && $(this).val() != 'undefined'){
  //     $.get('/label_packages?id='+$(this).val(), function(result){
  //       $('.label-package-container').html(result);
  //       $('.label-package-container .select2').select2();
  //     })
  //   }
  // });

  if($(".block-container").length > 0){ var role_form = $(".block-container").last().clone();
    $('#add_more a').on('click', function(){
      html = "<div class=\"block-container\">"+ role_form.html() + "</div>";
      $(".add_block").append(html);
      last_update = $(".add_block").children().last();
      last_update.find(".deleteable-field").remove()
      last_update.find(".bootstrap-select, .select2-container").remove()
      last_update.find("select").show()
      last_update.find('select:not(.selectpicker,.uniformjs,.import-schema-fieldsm,.service-point-select)').select2()
      return false;
    });
  }

});

// CHECK CATEGORIES
var rent = $('#property_category_disewa');
var sell = $('#property_category_dijual');
var pawning = $('#property_category_digadai');
var auction_property = $('#property_category_dilelang');

function propertyCheck(){
  rent.click(function(){
    checkType(this);
  });

  sell.click(function(){
    checkType(this);
  });

  pawning.click(function(){
    checkType(this);
  });

  auction_property.click(function(){
    checkType(this);
  });

  if($(rent).length > 0 || $(sell).length > 0 || $(pawning).length > 0 || $(auction_property).length > 0){
    checkType(rent[0]);
    checkType(sell[0]);
    checkType(pawning[0]);
    checkType(auction_property[0]);
  }
}


// FUNCTION CONFIG

function checkType(type){
  if($(type).is(':checked')){
    if(type == rent[0]){
      $('.amenity').show();
    }else{
      $('.amenity').hide();
    }
  }
}

function previewFile(input, imageHeader) {
  var preview = imageHeader[0];
  var file    = input.files[0];
  var reader  = new FileReader();

  reader.onloadend = function () {
    preview.src = reader.result;
  }

  if (file) {
    reader.readAsDataURL(file);
  } else {
    preview.src = "";
  }
}

function SellCategoriesSelected(cate){
  if(cate == 'Rumah'){
    $('.basic-form').appendTo('.hidden-form-basic');
    $('.basic-form-house').appendTo('.section-basic-form');
    $('.basic-form-house').show();
  }else if(cate == 'Apartemen'){
    $('.basic-form').appendTo('.hidden-form-basic');
    $('.basic-form-apartment').appendTo('.section-basic-form');
    $('.basic-form-apartment').show();
  }else if(cate == 'Ruko'){
    $('.basic-form').appendTo('.hidden-form-basic');
    $('.basic-form-ruko').appendTo('.section-basic-form');
    $('.basic-form-ruko').show();
  }else if(cate == 'Tanah'){
    $('.basic-form').appendTo('.hidden-form-basic');
    $('.basic-form-land').appendTo('.section-basic-form');
    $('.basic-form-land').show();
  }else if(cate == 'Perkantoran'){
    $('.basic-form').appendTo('.hidden-form-basic');
    $('.basic-form-office').appendTo('.section-basic-form');
    $('.basic-form-office').show();
  }else if(cate == 'Villa'){
    $('.basic-form').appendTo('.hidden-form-basic');
    $('.basic-form-warehouse').appendTo('.section-basic-form');
    $('.basic-form-warehouse').show();
  }
}

function RentCategoriesSelected(cate){
  if(cate == 'Rumah'){
    $('.basic-form').appendTo('.hidden-form-basic');
    $('.rent-basic-form-house').appendTo('.section-basic-form');
    $('.rent-basic-form-house').show();
  }else if(cate == 'Apartemen'){
    $('.basic-form').appendTo('.hidden-form-basic');
    $('.rent-basic-form-apartment').appendTo('.section-basic-form');
    $('.rent-basic-form-apartment').show();
  }else if(cate == 'Ruko'){
    $('.basic-form').appendTo('.hidden-form-basic');
    $('.rent-basic-form-ruko').appendTo('.section-basic-form');
    $('.rent-basic-form-ruko').show();
  }else if(cate == 'Perkantoran'){
    $('.basic-form').appendTo('.hidden-form-basic');
    $('.rent-basic-form-office').appendTo('.section-basic-form');
    $('.rent-basic-form-office').show();
  }else if(cate == 'Villa'){
    $('.basic-form').appendTo('.hidden-form-basic');
    $('.rent-basic-form-warehouse').appendTo('.section-basic-form');
    $('.rent-basic-form-warehouse').show();
  }
}

function RegionChangeFormat(){
  $('.provinces_select').on('change', function(){
    if($(this).val() != '' && $(this).val() != 'undefined'){
      $.get('/cities?id='+$(this).val()+'&node='+$(this).data("node"), function(result){
        $('.city-container').html(result);
        $('.city-container .select2').select2();
        if($('.city-container').hasClass('hidden')){
          $('.city-container').removeClass('hidden')
        }
      })
    }
  });

  $('.provinces_select_tag').on('change', function(){
    if($(this).val() != '' && $(this).val() != 'undefined'){
      $.get('/cities?id='+ $(this).val(), function(result){
        $('.city-container').html(result);
        $('.city-container .select2').select2();
      })
    }
  })

  $('#form-address').on('change', '.cities_select', function(){
    if($(this).val() != '' && $(this).val() != 'undefined'){
      $.get('/subdistricts?id='+$(this).val()+'&node='+$(this).data("node"), function(result){
        $('.sub-district-container').html(result);
        $('.sub-district-container .select2').select2();
      })
    }
  });

  $('.padd').on('change', '.cities_select', function(){
    if($(this).val() != '' && $(this).val() != 'undefined'){
      $.get('/subdistricts?id='+$(this).val()+'&node='+$(this).data("node"), function(result){
        $('.sub-district-container').html(result);
        $('.sub-district-container .select2').select2();
        if($('.sub-district-container').hasClass('hidden')){
          $('.sub-district-container').removeClass('hidden')
        }
      })
    }
  });

  $('#form-address').on('change', '.sub_districts_select', function(){
    if($(this).val() != '' && $(this).val() != 'undefined'){
      $.get('/village?id='+$(this).val()+'&node='+$(this).data("node"), function(result){
        $('.village-container').html(result);
        $('.village-container .select2').select2();
      })
    }
  });

  // $('#form-address').on('change', '.village_select', function(){
  //   if($(this).val() != '' && $(this).val() != 'undefined'){
  //     $.get('/area?id='+$(this).val()+'&node='+$(this).data("node"), function(result){
  //       $('.area-container').html(result);
  //       $('.area-container .select2').select2();
  //     })
  //   }
  // });

  // $('#form-address').on('change', '.area_select', function(){
  //   if($(this).val() == 'others'){
  //     $('#area_name').removeClass('hide');
  //     $('#area_name').attr('require', 'true');
  //   }else{
  //     if(!$('#area_name').hasClass('hide')){
  //       $('#area_name').addClass('hide');
  //     }
  //   }
  // });

  // $('.padd').on('change', '.sub_districts_select', function(){
  //   if($(this).val() != '' && $(this).val() != 'undefined'){
  //     $.get('/village?id='+$(this).val()+'&node='+$(this).data("node"), function(result){
  //       $('.village-container').html(result);
  //       $('.village-container .select2').select2();
  //       if($('.village-container').hasClass('hidden')){
  //         $('.village-container').removeClass('hidden')
  //       }
  //     })
  //   }
  // });
}

function CheckFacility(category){
  $.each($('.facility'), function(index, element){
    var url = $(this).data('url') + "?category=" + category;
    var that = $(this);
    $.ajax({
      type: "GET",
      url: url,
      success: function(data){
        $(that).html(data);
      }
    });
  });
}
