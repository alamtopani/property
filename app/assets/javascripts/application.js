//= require frontend/jquery-2.1.3.min
//= require jquery_ujs
//= require frontend/bootstrap.min
//= require frontend/jquery-ui.min
//= require frontend/jquery-scrolltofixed-min
//= require frontend/pinterest-grid-plugin
//= require frontend/lightbox
//= require frontend/gmap3
//= require frontend/shareds/carousel
//= require frontend/select2
//= require frontend/create_ads
//= require frontend/jquery.custom_ligthbox
//= require frontend/searching_properties
//= require frontend/jquery.downCount
//= require cocoon
//= require shareds/markerclusterer
//= require autocomplete-rails
//= require moment
//= require bootstrap-datetimepicker
//= require jquery.raty
//= require ratyrate
//= require frontend/flipster
//= require ahoy
//= require autonumeric
//= require analytics
//= require frontend/jquery.nicescroll

//= require v1/revolution.min
//= require v1/theme
//= require v1/theme_blog
//= require v1/flexslider
//= require v1/owl.carousel
//= require v1/doubletaptogo.min


$(window).load(function(){
  $('.search-form-multiaction li a').each(function(){
    $(this).click(function(){
      $('.h-search-multiaction').html($(this).attr('data-value'));
    });
  });

  $(".nav_trigger").click(function() {
    $("body").toggleClass("show_sidebar");
    $(".nav_trigger .fa").toggleClass("fa-navicon fa-times"); // toggle 2 classes in Jquery: http://goo.gl/3uQAFJ - http://goo.gl/t6BQ9Q
  });
});

$(document).ready(function(){
  $( '#nav-property li:has(ul)' ).doubleTapToGo();

  $(".list-side-property").niceScroll();
  $(".flipster").flipster({ style: 'carousel', start: 0 });
  $('#region_display_name').bind('railsAutocomplete.select', function(event, data){
    if($(this).parent().attr('class') != 'input-group'){
      $("#property-search").submit();
      document.search.submit.click();
    }
  });

  if($(window).width() < 990){
    if($(".property").length > 0){
      $.each($('.property'), function(index, element){
        action = element.dataset.action;
        $(element).attr('href', action);
      });
    }
  }

  $('.box-listing-property-auction').each(function(index){
    var expired = $(this).attr('data-expired');
    $(this).find('.countdown').downCount({
      date: expired
    }, function () {
      $(this).find('.countdown').text('Already TimeOut');
    });
  });

  $('.datepicker').datetimepicker({
    format: 'YYYY-MM-DD'
  });

  if($('.property-detail-countdown').attr('data-expired') != ""){
    $('.property-detail-countdown').each(function(index){
      var expired = $(this).attr('data-expired');
      $(this).find('.countdown-timer').downCount({
        date: expired
      }, function () {
        alert('Already TimeOut!');
      });
    });
  }

  $(".click-feedback").click(function () {
    $(".feedback-content").toggle("slide", {
        direction: "left"
    }, 100);
  });

  $( ".whislist-active" ).each(function(index) {
    $(this).on("click", function(){
      $.ajax({
        url: $(this).attr("url"),
        type: 'get',
        dataType: 'html',
        async: false,
        success: function(data) {
          result = data;
        }
      });

      return $(this).parents('.section-whistlist').html('<div class="whislist-active active"><i class="fa fa-heart"></i></div>');
    });
  });

  $('[data-toggle="popover_info"]').popover();
  $(".pop-top").popover({placement : 'top'});
  $(".pop-right").popover({placement : 'right'});
  $(".pop-bottom").popover({placement : 'bottom'});
  $(".pop-left").popover({ placement : 'left'});

  carousel();
  ahoy.trackView();

  $('.region-site-map li').click(function(){
    $(this).find('input').prop('checked', true);
    $('.search-region').submit();
  });

  $('.input-id-card').on('change', function(index){
    if($(this).length){
      previewFile(this, $('.id-card'));
    }
  });
});


function carousel(){
  $('#carousel-product').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    itemWidth: 140,
    itemMargin: 5,
    asNavFor: '#slider-product'
  });

  $('#slider-product').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    sync: "#carousel-product"
  });
}
