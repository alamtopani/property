$(window).load(function(){
	$(window).resize(function(){
    $('.home-carousel .item').each(function(){
      var topSlider = $('.home-carousel').outerWidth() / 1.5;
      $(this).css('height', topSlider);
    });

    $('.pop-property-carousel .item').each(function(){
      var topSlider = $('.pop-property-carousel').outerWidth() / 2.2;
      $(this).css('height', topSlider);
    });
  }).resize();
});

$(document).ready(function() {
  $("#transition-timer-carousel").on("slide.bs.carousel", function(event) {
    $(".transition-timer-carousel-progress-bar", this)
        .removeClass("animate").css("width", "0%");
  }).on("slid.bs.carousel", function(event) {
    $(".transition-timer-carousel-progress-bar", this)
        .addClass("animate").css("width", "100%");
  });
  $(".transition-timer-carousel-progress-bar", "#transition-timer-carousel")
      .css("width", "100%");

  $('.project-carousel-side').each(function(){
    $(this).carousel({interval: false, cycle: true});

    var projectSide = $(this).outerWidth() / 2;
    $(this).find('.item').css('height', projectSide);
  });
});
