$(window).load(function(){
  $(this).tooltip();

  // media carousel suported start
  $('.supporteds-item').each(function(i) {
    if( i % 6 == 0 ) {
      $(this).nextAll().andSelf().slice(0,6).wrapAll('<div class="item"></div>');
    }
  });

  $('.carousel_supporteds .item:first-child').addClass('active');
  // media carousel suported end

  // media carousel featured property start
  $('.featured-properties-item').each(function(i) {
    if( i % 4 == 0 ) {
      $(this).nextAll().andSelf().slice(0,4).wrapAll('<div class="item"></div>');
    }
  });

  $('.carousel_featured_properties .item:first-child').addClass('active');
  // media carousel featured property end

  $('.click-search-more-form').click(function(){
    $('.search-more-form').toggle(700);
  });

  $('.click-block-share-social').click(function(){
    $('.block-share-social').toggle(100);
  });

  $(".click-feedback").click(function () {
    $(".feedback-content").toggle("slide", {
        direction: "left"
    }, 100);
  });

  $( ".click-wishlist" ).each(function(index) {
    $(this).on("click", function(){
      $.ajax({
        url: $(this).attr("url"),
        type: 'get',
        dataType: 'html',
        async: false,
        success: function(data) {
            result = data;
        }
      });

      return $(this).parents('.section-whistlist').find('.status-whistlist').html('<i class="fa fa-heart set active"></i>');
    });
  });

  toChangeImg();
  toLightbox();
  selectCustom();
  changeRegion();
  navScrollFixed();
  Paralax();
  ScrollTopUp();
  ConfigurationOverview();
});

function toChangeImg(){
  $('.change-ava').on('change', function(index){
    if($(this).length){
      previewFile(this, $('.image-change'));
    }
  });
}

function previewFile(input, imageHeader) {
  var preview = imageHeader[0];
  var file    = input.files[0];
  var reader  = new FileReader();

  reader.onloadend = function () {
    preview.src = reader.result;
  }

  if (file) {
    reader.readAsDataURL(file);
  } else {
    preview.src = "";
  }
}

function toLightbox(){
  $(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
    event.preventDefault();
    $(this).Lightbox();
  });
}

function selectCustom(){
  $(".select2").select2({
    maximumSelectionLength: 2
  });
}

function changeRegion(){
  $('.provinces_select').on('change', function(){
    if($(this).val() != '' && $(this).val() != 'undefined'){
      $.get('/cities?id='+$(this).val()+'&node='+$(this).data("node"), function(result){
        $('.city-container').html(result);
        $('.city-container .select2').select2();
      })
    }
  });

  $('#form-address').on('change', '.cities_select', function(){
    if($(this).val() != '' && $(this).val() != 'undefined'){
      $.get('/subdistricts?id='+$(this).val()+'&node='+$(this).data("node"), function(result){
        $('.sub-district-container').html(result);
        $('.sub_districts_select .select2').select2();
      })
    }
  });

  $('#form-address').on('change', '.sub_districts_select', function(){
    if($(this).val() != '' && $(this).val() != 'undefined'){
      $.get('/village?id='+$(this).val()+'&node='+$(this).data("node"), function(result){
        $('.village-container').html(result);
        $('.village_select .select2').select2();
      })
    }
  });

  $('#form-address').on('change', '.village_select', function(){
    if($(this).val() != '' && $(this).val() != 'undefined'){
      $.get('/area?id='+$(this).val()+'&node='+$(this).data("node"), function(result){
        $('.area-container').html(result);
        $('.area_select .select2').select2();
      })
    }
  });

  // $('#form-address').on('change', '.area_select', function(){
  //   if($(this).val() == 'others'){
  //     $('#area_name').removeClass('hide');
  //     $('#area_name').attr('require', 'true');
  //   }
  // })
}

function navScrollFixed(){
  $('.navbar').scrollToFixed();
  $('.nav-project a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
}

function Paralax(){
  var $bgobj = $(".ha-bg-parallax"); // assigning the object
  $(window).on("scroll", function () {
    var yPos = -($(window).scrollTop() / $bgobj.data('speed'));
    var coords = '100% ' + yPos + 'px';
    $bgobj.css({ backgroundPosition: coords });
  });
}

function ScrollTopUp(){
  var offset=220;
  var duration=500;
  jQuery(window).scroll(function(){
    if(jQuery(this).scrollTop()>offset){
      jQuery('.toup').css({opacity:"1",display:"block",});
    }else{
      jQuery('.toup').css('opacity','0');
    }
  });

  jQuery('.toup').click(function(event){
    event.preventDefault();
    jQuery('html, body').animate({scrollTop:0},duration);
    return false;
  });
}

function ConfigurationOverview(){
  var cick_configuration_1 = $('.configuration_1');
  var cick_configuration_2 = $('.configuration_2');
  var cick_configuration_3 = $('.configuration_3');
  var cick_configuration_4 = $('.configuration_4');
  var cick_configuration_5 = $('.configuration_5');
  var cick_configuration_6 = $('.configuration_6');
  var click_file_2d = $('.click_file_2d');
  var click_file_3d = $('.click_file_3d');

  cick_configuration_1.click(function(){
    $('.configurations').hide();
    $('.configurations.configuration_1').show();
  });

  cick_configuration_2.click(function(){
    $('.configurations').hide();
    $('.configurations.configuration_2').show();
  });

  cick_configuration_3.click(function(){
    $('.configurations').hide();
    $('.configurations.configuration_3').show();
  });

  cick_configuration_4.click(function(){
    $('.configurations').hide();
    $('.configurations.configuration_4').show();
  });

  cick_configuration_5.click(function(){
    $('.configurations').hide();
    $('.configurations.configuration_5').show();
  });

  cick_configuration_6.click(function(){
    $('.configurations').hide();
    $('.configurations.configuration_6').show();
  });

  click_file_2d.click(function(){
    $(this).parents('.room_type').find('.cover_file_2d').show();
    $(this).parents('.room_type').find('.cover_file_3d').hide();
  });

  click_file_3d.click(function(){
    $(this).parents('.room_type').find('.cover_file_2d').hide();
    $(this).parents('.room_type').find('.cover_file_3d').show();
  });
}

$(document).ready(function(){
  $('[data-toggle="popover_info"]').popover();
  $(".pop-top").popover({placement : 'top'});
  $(".pop-right").popover({placement : 'right'});
  $(".pop-bottom").popover({placement : 'bottom'});
  $(".pop-left").popover({ placement : 'left'});
});
