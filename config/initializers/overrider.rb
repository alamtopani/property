class String
  def clean_upcase
    self.clean_space.upcase
  end

  def clean_space
    self.gsub(/^\s{1,}|\s{1,}$/,'')
  end

  def clean_downcase
    self.clean_space.downcase
  end
end

class Numeric
  def round_up(nearest=10)
    self % nearest == 0 ? self : self + nearest - (self % nearest)
  end
  def round_down(nearest=10)
    self % nearest == 0 ? self : self - (self % nearest)
  end

  def clean_space
    self
  end
end

class Array
  def clean_space
    self.inject([]){|array,str| array.push(str.clean_space) if str.clean_space.present?;array}
  end

  def included_in? array
    array.to_set.superset?(self.to_set)
  end
end

module ActionDispatch
  module Http
    class UploadedFile
      def to_hash
        {
          path: tempfile.path,
          original_filename: original_filename,
          content_type: content_type,
          headers: headers
        }
      end
    end
  end
end
