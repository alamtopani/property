role :app, %w{rumahdaku@107.155.108.192}
role :web, %w{rumahdaku@107.155.108.192}
role :db,  %w{rumahdaku@107.155.108.192}

set :stage, :production

# Replace 127.0.0.1 with your server's IP address!
server '107.155.108.192', user: 'rumahdaku', roles: %w{web app}

set :rbenv_type,     :system
set :rbenv_ruby,     '2.2.2'
set :rbenv_prefix,   "RBENV_ROOT=#{fetch(:rbenv_path)} RBENV_VERSION=#{fetch(:rbenv_ruby)} #{fetch(:rbenv_path)}/bin/rbenv exec"
set :rbenv_map_bins, %w{rake gem bundle ruby rails}
set :rbenv_roles,    :all

set :deploy_to,       "/home/rumahdaku/www"
set :rails_env,       "production"
set :branch,          "production"

# set :sidekiq_concurrency, 20
# set :sidekiq_queue, ['default']
# set :sidekiq_timeout, 300
# set :sidekiq_pid,  File.join(shared_path, 'tmp', 'pids', 'sidekiq.pid')
# set :sidekiq_env,  fetch(:rack_env, fetch(:rails_env, fetch(:stage)))
# set :sidekiq_log,  File.join(shared_path, 'log', 'sidekiq.log')

set :unicorn_config_path, "/home/rumahdaku/www/current/config/unicorn.rb"
