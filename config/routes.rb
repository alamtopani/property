Rails.application.routes.draw do
  post '/rate' => 'rater#create', :as => 'rate'
  mount Ckeditor::Engine => '/ckeditor'
  devise_for :users,
    controllers: {
      omniauth_callbacks: 'omniauth_callbacks',
      registrations: 'registrations',
      sessions: 'sessions'
    },
    path_names: {
      sign_in:  'login',
      sign_out: 'logout',
      sign_up:  'register'
    }

  root 'publics#home'

  resources :properties do
    collection do
      get :auction
      get :featureds, to: 'properties#featured'
      get 'wishlist/:id', to: 'properties#wishlist', as: 'wishlist'
      post :checkout_bid
    end
    member do
      get 'reviews', to: 'properties#reviews'
      delete 'reviews/:rate_id', to: 'properties#delete_reviews', as: 'delete_rate'
      post 'reviews', to: 'reviews#create'
    end
  end

  resources :users, only: [:show]
  resources :landing_pages, only: [:show]
  resources :inquiries
  resources :feedbacks, only: [:create]
  resources :reports, only: [:create]
  resources :schedule_visits, only: [:create]
  resources :confirmations, path: 'konfirmasi', only: [:new, :create]

  get 'map',                          to: 'properties#map',             as: 'map'
  get 'list',                         to: 'properties#list',            as: 'list'
  get 'package',                      to: 'publics#package',            as: 'package'
  get 'get_societies',                to: 'publics#get_societies',      as: 'get_societies'

  get 'get_assets',                   to: 'xhrs#get_assets',            as: 'get_assets'
  get 'get_assets_property',          to: 'xhrs#get_assets_property',   as: 'get_assets_property'
  post 'save_assets_property',        to: 'xhrs#save_assets_property',  as: 'save_assets_property'
  get 'get_categories',               to: 'publics#get_categories',     as: 'get_categories'
  get 'cities_tag',                   to: 'publics#cities_tag',         as: 'cities_tag'
  get 'cities',                       to: 'xhrs#cities',                as: 'cities'
  get 'subdistricts',                 to: 'xhrs#subdistricts',          as: 'subdistricts'
  get 'village',                      to: 'xhrs#village',               as: 'village'
  get 'area',                         to: 'xhrs#area',                  as: 'area'
  get 'get_by_city',                  to: 'xhrs#get_by_city',           as: 'get_by_city'
  get 'get_properties_search',        to: 'xhrs#get_properties_search', as: 'get_properties_search'
  get '/get_detail_profile_property', to: 'xhrs#get_detail_properties', as: 'get_detail_profile_property'
  get '/label_packages',              to: 'xhrs#label_packages',        as: 'label_packages'

  get "autocomplete_region",          to: "xhrs#autocomplete_region"
  get "autocomplete_auction_region",  to: "xhrs#autocomplete_auction_region"

  get 'region/:id',                   to: 'landing_pages#region',       as: 'region'
  get 'contact',                      to: 'publics#contact',            as: 'contact'
  get 'help',                         to: 'publics#help',               as: 'help'
  put 'daftar-paket/:id',             to: 'publics#add_package',        as: 'add_package'
  
  get "sitemap.xml" => "sitemaps#index", format: "xml",                 as: 'sitemap'

  resources :blog_infos, only: [:index, :show], path: 'blog' do
    member do
      get :category
    end
    collection do
      get :results
    end
  end
  resources :agents
  resources :xhrs do
    collection do
      get :get_categories
      get :get_facilities
    end
  end

  namespace :backend do
    # The Role
    resources :roles, except: :show do
      patch 'change', on: :member
      resources :sections, controller: 'role_sections', only: :none do
        collection do
          post :create
          post :create_rule
          post :create_field
        end

        member do
          put :rule_on
          put :rule_off

          delete :destroy
          delete :destroy_rule
        end
      end
    end

    get 'dashboard', to: 'home#dashboard', as: 'dashboard'
    resources :admins
    resources :members do
      collection do
        post :do_multiple_act
      end
    end
    resources :agents do
      collection do
        post :do_multiple_act
      end
    end
    resources :packages do
      collection do
        post :do_multiple_act
      end
    end
    resources :order_user_packages do
      member do
        patch '/confirmations', to: 'order_user_packages#confirmations'
      end
      collection do
        post '/multiple_confirmations', to: 'order_user_packages#multiple_confirmations'
      end
    end
    resources :developers do
      collection do
        post :do_multiple_act
      end
    end
    resources :properties do
      collection do
        get :pending
        post :do_multiple_act
        get :import, to: 'properties#import'
        post :do_import, to: 'properties#do_import'
        post :import, to: 'properties#do_import'
      end
    end
    resources :auction_properties do
      collection do
        post :do_multiple_act
      end
    end
    resources :property_types do
      collection do
        post :do_multiple_act
      end
    end
    resources :asset_facilities do
      collection do
        post :do_multiple_act
      end
    end
    resources :locations do
      collection do
        post :do_multiple_act
      end
    end
    resources :supporteds do
      collection do
        post :do_multiple_act
      end
    end
    resources :testimonials do
      collection do
        post :do_multiple_act
      end
    end
    resources :feedbacks do
      collection do
        post :do_multiple_act
      end
    end
    resources :reports do
      collection do
        post :do_multiple_act
      end
    end
    resources :sold_reports do
      collection do
        post :do_multiple_act
      end
    end
    resources :blog_infos do
      collection do
        post :do_multiple_act
      end
    end
    resources :category_blog_infos do
      collection do
        post :do_multiple_act
      end
    end
    resources :regions
    resources :landing_pages
    resources :web_settings

    resources :confirmations do
      collection do
        post :do_multiple_act
      end
    end

    resources :order_packages do
      collection do
        post :do_multiple_act
      end
    end
  end

  namespace :userpage do
    get 'dashboard', to: 'home#dashboard', as: 'dashboard'
    resources :members do
      member do
        put :upgrade
      end
    end
    resources :agents, only: [:edit, :update, :show]
    resources :wishlists
    resources :inquiries
    resources :notifications
    resources :properties do
      member do
        put :renew
      end
      collection do
        post :do_report_sold
      end
    end
    resources :sold_reports
    resources :order_packages
    resources :packages do
      collection do
        get :invoices
      end
      member do
        get :invoice
      end
    end
  end

end
