module SeedRegion
  REGIONS =  {
    "Aceh"=> [
      "Aceh Barat"=>["Johan Pahlawan", "Samatiga", "Bubon", "Arongan Lambalek", "Woyla", "Woyla Barat", "Woyla Timur", "Kaway Xvi", "Meureubo", "Pantai Ceuremen", "Panton Reu", "Sungai Mas"],
      "Aceh Barat Daya"=>["Manggeng", "Lembah Sabil", "Tangan-tangan", "Setia", "Blang Pidie", "Jeumpa", "Susoh", "Kuala Batee", "Babah Rot"],
      "Aceh Besar"=>["Lhoong", "Lhoknga", "Leupung", "Indrapuri", "Kuta Cot Glie", "Seulimeum", "Kota Jantho", "Lembah Seulawah", "Mesjid Raya", "Darussalam", "Baitussalam", "Kuta Baro",
                     "Montasik", "Blang Bintang", "Ingin Jaya", "Krueng Barona Jaya", "Suka Makmur", "Kuta Malaka", "Simpang Tiga", "Darul Imarah", "Darul Kamal", "Peukan Bada", "Pulo Aceh"],
      "Aceh Jaya"=>["Teunom", "Pasie Raya", "Panga", "Krueng Sabee", "Setia Bakti", "Sampoiniet", "Darul Hikmah", "Jaya", "Indra Jaya"],
      "Aceh Selatan"=>["Trumon", "Trumon Timur", "Trumon Tengah", "Bakongan", "Bakongan Timur", "Kota Bahagia", "Kluet Selatan", "Kluet Timur", "Kluet Utara", "Pasie Raja", "Kluet Tengah",
                       "Tapak Tuan", "Sama Dua", "Sawang", "Meukek", "Labuhan Haji", "Labuhan Haji Timur", "Labuhan Haji Barat"],
      "Aceh Singkil"=>["Pulau Banyak", "Pulau Banyak Barat", "Singkil", "Singkil Utara", "Kuala Baru", "Simpang Kanan", "Gunung Meriah", "Danau Paris", "Suro", "Singkohor", "Kota Baharu"],
      "Aceh Tamiang"=>["Tamiang Hulu", "Bandar Pusaka", "Kejuruan Muda", "Tenggulun", "Rantau", "Kota Kuala Simpang", "Seruway", "Bendahara", "Banda Mulia", "Karang Baru", "Sekerak", "Manyak Payed"],
      "Aceh Tengah"=>["Linge", "Atu Lintang", "Jagong Jeget", "Bintang", "Lut Tawar", "Kebayakan", "Pegasing", "Bies", "Bebesen", "Kute Panang", "Silih Nara", "Ketol", "Celala", "Rusip Antara"],
      "Aceh Tenggara"=>["Lawe Alas", "Babul Rahmad", "Tanoh Alas", "Lawe Sigala-gala", "Babul Makmur", "Semadam", "Lauser", "Bambel", "Bukit Tusam", "Lawe Sumur", "Babussalam", "Lawe Bulan", "Badar",
                        "Darul Hasanah", "Ketambe", "Deleng Pokhkisen"],
      "Aceh Timur"=>["Serba Jadi", "Simpang Jernih", "Peunaron", "Birem Bayeun", "Rantau Selamat", "Sungai Raya", "Peureulak", "Peureulak Timur", "Peureulak Barat", "Ranto Peureulak", "Idi Rayeuk",
                     "Peudawa", "Banda Alam", "Idi Tunong", "Darul Ihsan", "Idi Timur", "Darul Aman", "Nurussalam", "Darul Falah", "Julok", "Indra Makmur", "Pante Bidari", "Simpang Ulim", "Madat"],
      "Aceh Utara"=>["Sawang", "Nisam", "Nisam Antara", "Banda Baro", "Kuta Makmur", "Simpang Keramat", "Syamtalira Bayu", "Geureudong Pase", "Meurah Mulia", "Matangkuli", "Paya Bakong",
                     "Pirak Timu", "Cot Girek", "Tanah Jambo Aye", "Langkahan", "Seunudon", "Baktiya", "Baktiya Barat", "Lhoksukon", "Tanah Luas", "Nibong", "Samudera", "Syamtalira Aron",
                     "Tanah Pasir", "Lapang", "Muara Batu", "Dewantara"],
      "Bener Meriah"=>["Timang Gajah", "Gajah Putih", "Pintu Rime Gayo", "Bukit", "Wih Pesam", "Bandar", "Bener Kelipah", "Syiah Utama", "Mesidah", "Permata"],
      "Bireuen"=>["Samalanga", "Simpang Mamplam", "Pandrah", "Jeunieb", "Peulimbang", "Peudada", "Juli", "Jeumpa", "Kota Juang", "Kuala", "Jangka", "Peusangan", "Peusangan Selatan",
                  "Peusangan Siblah Krueng", "Makmur", "Ganda Pura", "Kuta Blang"],
      "Gayo Lues"=>["Kuta Panjang", "Blang Jerango", "Blangkejeren", "Putri Betung", "Dabun Gelang", "Blang Pegayon", "Pining", "Rikit Gaib", "Pantan Cuaca", "Terangun", "Tripe Jaya"],
      "Nagan Raya"=>["Darul Makmur", "Tripa Makmur", "Kuala", "Kuala Pesisir", "Tadu Raya", "Beutong", "Beutong Ateuh Banggalang", "Seunagan", "Suka Makmue", "Seunagan Timur"],
      "Pidie"=>["Geumpang", "Mane", "Glumpang Tiga", "Glumpang Baro", "Mutiara", "Mutiara Timur", "Tiro/truseb", "Tangse", "Keumala", "Titeue", "Sakti", "Mila", "Padang Tiji",
                "Delima", "Grong Grong", "Indrajaya", "Peukan Baro", "Kembang Tanjong", "Simpang Tiga", "Kota Sigli", "Pidie", "Batee", "Muara Tiga"],
      "Pidie Jaya"=>["Meureudu", "Meurah Dua", "Bandar Dua", "Jangka Buya", "Ulim", "Trienggadeng", "Panteraja", "Bandar Baru"],
      "Simeulue"=>["Teupah Selatan", "Simeulue Timur", "Teupah Barat", "Teupah Tengah", "Simeulue Tengah", "Teluk Dalam", "Simeulue Cut", "Salang", "Simeulue Barat", "Alafan"],
      "Banda Aceh"=>["Meuraxa", "Jaya Baru", "Banda Raya", "Baiturrahman", "Lueng Bata", "Kuta Alam", "Kuta Raja", "Syiah Kuala", "Ulee Kareng"],
      "Langsa"=>["Langsa Timur", "Langsa Lama", "Langsa Barat", "Langsa Baro", "Langsa Kota"],
      "Lhokseumawe"=>["Blang Mangat", "Muara Dua", "Muara Satu", "Banda Sakti"],
      "Sabang"=>["Sukajaya", "Sukakarya"],
      "Subulussalam"=>["Simpang Kiri", "Penanggalan", "Rundeng", "Sultan Daulat", "Longkib"]
    ],

    "Sumatera Utara"=> [
      "Asahan"=>["Bandar Pasir Mandoge", "Bandar Pulau", "Aek Songsongan", "Rahuning", "Pulau Rakyat", "Aek Kuasan", "Aek Ledong", "Sei Kepayang", "Sei Kepayang Barat",
                 "Sei Kepayang Timur", "Tanjung Balai", "Simpang Empat", "Teluk Dalam", "Air Batu", "Sei Dadap", "Buntu Pane", "Tinggi Raja", "Setia Janji", "Meranti",
                  "Pulo Bandring", "Rawang Panca Arga", "Air Joman", "Silau Laut", "Kisaran Barat", "Kisaran Timur"],
      "Batubara"=>["Sei Balai", "Tanjung Tiram", "Talawi", "Limapuluh", "Air Putih", "Sei Suka", "Medang Deras"],
      "Dairi"=>["Sidikalang", "Berampu", "Sitinjo", "Parbuluan", "Sumbul", "Silahi Sabungan", "Silima Pungga-pungga", "Lae Parira", "Siempat Nempu", "Siempat Nempu Hulu",
                "Siempat Nempu Hilir", "Tiga Lingga", "Gunung Sitember", "Pegagan Hilir", "Tanah Pinem"],
      "Deli Serdang"=>["Gunung Meriah", "Sinembah Tanjung Muda Hulu", "Sibolangit", "Kutalimbaru", "Pancur Batu", "Namo Rambe", "Biru-biru", "Sinembah Tanjung Muda Hilir",
                       "Bangun Purba", "Galang", "Tanjung Morawa", "Patumbak", "Deli Tua", "Sunggal", "Hamparan Perak", "Labuhan Deli", "Percut Sei Tuan", "Batang Kuis",
                       "Pantai Labu", "Beringin", "Lubuk Pakam", "Pagar Merbau"],
      "Humbang Hasundutan"=>["Pakkat", "Onan Ganjang", "Sijama Polang", "Dolok Sanggul", "Lintong Nihuta", "Paranginan", "Bakti Raja", "Pollung", "Parlilitan", "Tara Bintang"],
      "Karo"=>[Mardingding, Laubaleng, Tiga Binanga, Juhar, Munte, Kuta Buluh, Payung, Tiganderket, Simpang Empat, Naman Teran, Merdeka, Kabanjahe, Berastagi, Tigapanah, Dolat Rayat, Merek, Barusjahe],      "Labuhanbatu"=>[ Bilah Hulu, Pangkatan, Bilah Barat, Bilah Hilir, Panai Hulu, Panai Tengah, Panai Hilir, Rantau Selatan, Rantau Utara],      "Labuhanbatu Selatan"=>[ Sungai Kanan, Torgamba, Kota Pinang, Silangkitang, Kampung Rakyat],      "Labuhanbatu Utara"=>[ Na Ix-x, Marbau, Aek Kuo, Aek Natas, Kualuh Selatan, Kualuh Hilir, Kualuh Hulu, Kualuh Leidong],      "Langkat"=>[ Bohorok, Sirapit, Salapian, Kutambaru, Sei Bingai, Kuala, Selesai, Binjai, Stabat, Wampu, Batang Serangan, Sawit Seberang, Padang Tualang, Hinai, Secanggang, Tanjung Pura, Gebang, Babalan, Sei Lepan, Brandan Barat, Besitang, Pangkalan Susu, Pematang Jaya],      "Mandailing Natal"=>["Batahan", "Sinunukan", "Batang Natal", "Lingga Bayu", "Ranto Baek", "Kotanopan", "Ulu Pungkut", "Tambangan", "Lembah Sorik Marapi", "Puncak Sorik Marapi",,                           "Muara Sipongi", "Pakantan", "Panyabungan", "Panyabungan Selatan", "Panyabungan Barat", "Panyabungan Utara", "Panyabungan Timur", "Huta Bargot", "Natal",,                           "Muara Batang Gadis", "Siabu", "Bukit Malintang", "Naga Juang"],      "Nias"=>["Idano Gawo", "Bawolato", "Ulugawo", "Gido", "Ma U", "Somolo - Molo", "Hiliduho", "Hili Serangkai", "Botomuzoi"],      "Nias Barat"=>[ Sirombu, Lahomi, Ulu Moro O, Lolofitu Moi, Mandrehe Utara, Mandrehe, Mandrehe Barat, Moro O],      "Nias Selatan"=>[ Hibala, Pulau-pulau Batu, Pulau-pulau Batu Timur, Teluk Dalam, Fanayama, Toma, Maniamolo, Mazino, Amandraya, Aramo, Lahusa, Gomo, Susua, Mazo, Umbunasi, Lolomatua, Lolowa/'u', Hilimegai],      "Nias Utara"=>[ Tugala Oyo, Alasa, Alasa Talu Muzoi, Namohalu Esiwa, Sitolu Ori, Tuhemberua, Sawo, Lotu, Lahewa Timur, Afulu, Lahewa],      "Padang Lawas"=>[ Sosopan, Ulu Barumun, Barumun, Barumun Selatan, Lubuk Barumun, Sosa, Batang Lubu Sutam, Huta Raja Tinggi, Huristak, Barumun Tengah, Aek Nabara Barumun, Sihapas Barumun],      "Padang Lawas Utara"=>[ Batang Onang, Padang Bolak Julu, Portibi, Padang Bolak, Simangambat, Halongonan, Dolok, Dolok Sigompulon, Hulu Sihapas],      "Pakpak Bharat"=>[ Salak, Sitellu Tali Urang Jehe, Pagindar, Sitellu Tali Urang Julu, Pergetteng-getteng Sengkut, Kerajaan, Tinada, Siempat Rube],      "Samosir"=>[ Sianjur Mula Mula, Harian, Sitio-tio, Onan Runggu, Nainggolan, Palipi, Ronggur Nihuta, Pangururan, Simanindo],      "Serdang Bedagai"=>[ Kotarih, Silinda, Bintang Bayu, Dolok Masihul, Serbajadi, Sipispis, Dolok Merawan, Tebingtinggi, Tebing Syahbandar, Bandar Khalipah, Tanjung Beringin, Sei Rampah, Sei Bamban, Teluk Mengkudu, Perbaungan, Pegajahan, Pantai Cermin],      "Simalungun"=>[ Silimakuta, Pematang Silimahuta, Purba, Haranggaol Horison, Dolok Pardamean, Sidamanik, Pematang Sidamanik, Girsang Sipangan Bolon, Tanah Jawa, Hatonduhan, Dolok Panribuan, Jorlang Hataran, Panei, Panombean Panei, Raya, Dolok Silau, Silau Kahean, Raya Kahean, Tapian Dolok, Dolok Batu Nanggar, Siantar, Gunung Malela, Gunung Maligas, Hutabayu Raja, Jawa Maraja Bah Jambi, Pematang Bandar, Bandar Huluan, Bandar, Bandar Masilam, Bosar Maligas, Ujung Padang],      "Tapanuli Selatan"=>["Batang Angkola", "Sayur Matinggi", "Tano Tombangan Angkola", "Angkola Timur", "Angkola Selatan", "Angkola  Barat", "Angkola Sangkunur", "Batang Toru",,                           "Marancar", "Muara Batang Toru", "Sipirok", "Arse", "Saipar Dolok Hole", "Aek Bilah"],      "Tapanuli Tengah"=>["Pinang Sori", "Badiri", "Sibabangun", "Lumut", "Sukabangun", "Pandan", "Tukka", "Sarudik", Tapian Nauli, Sitahuis, Kolang, Sorkam, Sorkam Barat, Pasaribu Tobing, Barus, Sosor Gadong, Andam Dewi, Barus Utara, Manduamas, Sirandorung],      "Tapanuli Utara"=>[ Parmonangan, Adian Koting, Sipoholon, Tarutung, Siatas Barita, Pahae Julu, Pahae Jae, Purbatua, Simangumban, Pangaribuan, Garoga, Sipahutar, Siborong-borong, Pagaran, Muara],      "Toba Samosir"=>[ Balige, Tampahan, Laguboti, Habinsaran, Borbor, Nassau, Silaen, Sigumpar, Porsea, Pintu Pohan Meranti, Siantar Narumonda, Parmaksian, Lumban Julu, Uluan, Ajibata, Bonatua Lunasi],      "Binjai"=>[ Binjai Selatan, Binjai Kota, Binjai Timur, Binjai Utara, Binjai Barat],      "Gunungsitoli"=>[ Gunungsitoli Idanoi, Gunungsitoli Selatan, Gunungsitoli Barat, Gunung Sitoli, Gunungsitoli Alo Oa, Gunungsitoli Utara],      "Medan"=>[ Medan Tuntungan, Medan Johor, Medan Amplas, Medan Denai, Medan Area, Medan Kota, Medan Maimun, Medan Polonia, Medan Baru, Medan Selayang, Medan Sunggal, Medan Helvetia, Medan Petisah, Medan Barat, Medan Timur, Medan Perjuangan, Medan Tembung, Medan Deli, Medan Labuhan, Medan Marelan, Medan Belawan],      "Padangsidempuan"=>[ Padangsidimpuan Tenggara, Padangsidimpuan Selatan, Padangsidimpuan Batunadua, Padangsidimpuan Utara, Padangsidimpuan Hutaimbaru, Padangsidimpuan Angkola Julu],      "Pematangsiantar"=>[ Siantar Marihat, Siantar Marimbun, Siantar Selatan, Siantar Barat, Siantar Utara, Siantar Timur, Siantar Martoba, Siantar Sitalasari],      "Sibolga"=>[ Sibolga Utara, Sibolga Kota, Sibolga Selatan, Sibolga Sambas],      "Tanjungbalai"=>[ Datuk Bandar, Datuk Bandar Timur, Tanjung Balai Selatan, Tanjung Balai Utara, Sei Tualang Raso, Teluk Nibung],      "Tebing Tinggi"=>[ Padang Hulu, Tebing Tinggi Kota, Rambutan, Bajenis, Padang Hilir],    ],

    "Bengkulu"=> [
      "Bengkulu Selatan",
      "Bengkulu Tengah",
      "Bengkulu Utara",
      "Kaur",
      "Kepahiang",
      "Lebong",
      "Mukomuko",
      "Rejang Lebong",
      "Seluma",
      "Bengkulu"
    ],

    "Jambi"=> [
      "Batanghari",
      "Bungo",
      "Kerinci",
      "Merangin",
      "Muaro Jambi",
      "Sarolangun",
      "Tanjung Jabung Barat",
      "Tanjung Jabung Timur",
      "Tebo",
      "Jambi",
      "Sungai Penuh"
    ],

    "Riau"=> [
      "Bengkalis",
      "Indragiri Hilir",
      "Indragiri Hulu",
      "Kampar",
      "Kuantan Singingi",
      "Pelalawan",
      "Rokan Hilir",
      "Rokan Hulu",
      "Siak",
      "Kepulauan Meranti",
      "Dumai",
      "Pekanbaru"
    ],

    "Sumatera Barat"=> [
      "Agam",
      "Dharmasraya",
      "Kepulauan Mentawai",
      "Lima Puluh Kota",
      "Padang Pariaman",
      "Pasaman",
      "Pasaman Barat",
      "Pesisir Selatan",
      "Sijunjung",
      "Solok",
      "Solok Selatan",
      "Tanah Datar",
      "Bukittinggi",
      "Padang",
      "Padangpanjang",
      "Pariaman",
      "Payakumbuh",
      "Sawahlunto"
    ],

    "Sumatera Selatan"=> [
      "Banyuasin",
      "Empat Lawang",
      "Lahat",
      "Muara Enim",
      "Musi Banyuasin",
      "Musi Rawas",
      "Ogan Ilir",
      "Ogan Komering Ilir",
      "Ogan Komering Ulu",
      "Ogan Komering Ulu Selatan",
      "Ogan Komering Ulu Timur",
      "Lubuklinggau",
      "Pagar Alam",
      "Palembang",
      "Prabumulih"
    ],

    "Lampung"=> [
      "Lampung Barat",
      "Lampung Selatan",
      "Lampung Tengah",
      "Lampung Timur",
      "Lampung Utara",
      "Mesuji",
      "Pesawaran",
      "Pringsewu",
      "Tanggamus",
      "Tulang Bawang",
      "Tulang Bawang Barat",
      "Way Kanan",
      "Bandar Lampung",
      "Metro"
    ],

    "Kepulauan Bangka Belitung"=> [
      "Bangka",
      "Bangka Barat",
      "Bangka Selatan",
      "Bangka Tengah",
      "Belitung",
      "Belitung Timur",
      "Pangkal Pinang"
    ],

    "Kepulauan Riau"=> [
      "Bintan",
      "Karimun",
      "Kepulauan Anambas",
      "Lingga",
      "Natuna",
      "Batam",
      "Tanjung Pinang"
    ],

    "Banten"=> [
      "Tangerang",
      "Serang",
      "Lebak",
      "Pandeglang",
      "Cilegon",
      "Tangerang Selatan"
    ],

    "Jawa Barat"=> [
      "Bandung",
      "Bandung Barat",
      "Bekasi",
      "Bogor",
      "Ciamis",
      "Cianjur",
      "Cirebon",
      "Depok",
      "Garut",
      "Indramayu",
      "Karawang",
      "Kuningan",
      "Majalengka",
      "Purwakarta",
      "Subang",
      "Sukabumi",
      "Sumedang",
      "Tasikmalaya",
      "Banjar",
      "Cimahi",
      "Depok",
      "Tasikmalaya"
    ],

    "DKI Jakarta"=> [
      "Kepulauan Seribu",
      "Jakarta Barat",
      "Jakarta Pusat",
      "Jakarta Selatan",
      "Jakarta Timur",
      "Jakarta Utara"
    ],

    "Jawa Tengah"=> [
      "Banjarnegara",
      "Banyumas",
      "Batang",
      "Blora",
      "Boyolali",
      "Brebes",
      "Cilacap",
      "Demak",
      "Grobogan",
      "Jepara",
      "Karanganyar",
      "Kebumen",
      "Kendal",
      "Klaten",
      "Kudus",
      "Magelang",
      "Pati",
      "Pekalongan",
      "Pemalang",
      "Purbalingga",
      "Purworejo",
      "Rembang",
      "Semarang",
      "Sragen",
      "Sukoharjo",
      "Tegal",
      "Temanggung",
      "Wonogiri",
      "Wonosobo",
      "Salatiga",
      "Surakarta",
      "Tegal"
    ],

    "Jawa Timur"=> [
      "Bangkalan",
      "Banyuwangi",
      "Blitar",
      "Bojonegoro",
      "Bondowoso",
      "Gresik",
      "Jember",
      "Jombang",
      "Kediri",
      "Lamongan",
      "Lumajang",
      "Madiun",
      "Magetan",
      "Malang",
      "Mojokerto",
      "Nganjuk",
      "Ngawi",
      "Pacitan",
      "Pamekasan",
      "Pasuruan",
      "Ponorogo",
      "Probolinggo",
      "Sampang",
      "Sidoarjo",
      "Situbondo",
      "Sumenep",
      "Trenggalek",
      "Tuban",
      "Tulungagung",
      "Batu",
      "Surabaya"
    ],

    "Yogyakarta"=> [
      "Bantul",
      "Gunung Kidul",
      "Kulon Progo",
      "Sleman",
      "Yogyakarta"
    ],

    "Bali"=> [
      "Badung",
      "Bangli",
      "Buleleng",
      "Gianyar",
      "Jembrana",
      "Karangasem",
      "Klungkung",
      "Tabanan",
      "Denpasar"
    ],

    "Nusa Tenggara Barat"=> [
      "Bima",
      "Dompu",
      "Lombok Barat",
      "Lombok Tengah",
      "Lombok Timur",
      "Lombok Utara",
      "Sumbawa",
      "Sumbawa Barat",
      "Mataram"
    ],

    "Nusa Tenggara Timur"=> [
      "Alor",
      "Belu",
      "Ende",
      "Flores Timur",
      "Kupang",
      "Lembata",
      "Manggarai",
      "Manggarai Barat",
      "Manggarai Timur",
      "Ngada",
      "Nagekeo",
      "Rote Ndao",
      "Sabu Raijua",
      "Sikka",
      "Sumba Barat",
      "Sumba Barat Daya",
      "Sumba Tengah",
      "Sumba Timur",
      "Timor Tengah Selatan",
      "Timor Tengah Utara",
      "Kupang"
    ],

    "Kalimantan Barat"=> [
      "Bengkayang",
      "Kapuas Hulu",
      "Kayong Utara",
      "Ketapang",
      "Kubu Raya",
      "Landak",
      "Melawi",
      "Pontianak",
      "Sambas",
      "Sanggau",
      "Sekadau",
      "Sintang",
      "Singkawang"
    ],

    "Kalimantan Selatan"=> [
      "Balangan",
      "Barito Kuala",
      "Hulu Sungai Selatan",
      "Hulu Sungai Tengah",
      "Hulu Sungai Utara",
      "Kotabaru",
      "Tabalong",
      "Tanah Bumbu",
      "Tanah Laut",
      "Tapin",
      "Banjarbaru",
      "Banjarmasin"
    ],

    "Kalimantan Tengah"=> [
      "Barito Selatan",
      "Barito Timur",
      "Barito Utara",
      "Gunung Mas",
      "Kapuas",
      "Katingan",
      "Kotawaringin Barat",
      "Kotawaringin Timur",
      "Lamandau",
      "Murung Raya",
      "Pulang Pisau",
      "Sukamara",
      "Seruyan",
      "Palangka Raya"
    ],

    "Kalimantan Timur"=> [
      "Berau",
      "Bulungan",
      "Kutai Barat",
      "Kutai Kartanegara",
      "Kutai Timur",
      "Malinau",
      "Nunukan",
      "Paser",
      "Penajam Paser Utara",
      "Tana Tidung",
      "Balikpapan",
      "Bontang",
      "Samarinda",
      "Tarakan"
    ],

    "Gorontalo"=> [
      "Boalemo",
      "Bone Bolango",
      "Gorontalo",
      "Gorontalo Utara",
      "Pohuwato",
      "Gorontalo"
    ],

    "Sulawesi Selatan"=> [
      "Bantaeng",
      "Barru",
      "Bone",
      "Bulukumba",
      "Enrekang",
      "Gowa",
      "Jeneponto",
      "Kepulauan Selayar",
      "Luwu",
      "Luwu Timur",
      "Luwu Utara",
      "Maros",
      "Pangkajene dan Kepulauan",
      "Pinrang",
      "Sidenreng Rappang",
      "Sinjai",
      "Soppeng",
      "Takalar",
      "Tana Toraja",
      "Toraja Utara",
      "Wajo",
      "Makassar",
      "Palopo",
      "Parepare"
    ],

    "Sulawesi Tenggara"=> [
      "Bombana",
      "Buton",
      "Buton Utara",
      "Kolaka",
      "Kolaka Utara",
      "Konawe",
      "Konawe Selatan",
      "Konawe Utara",
      "Muna",
      "Wakatobi",
      "Bau-Bau",
      "Kendari"
    ],

    "Sulawesi Tengah"=> [
      "Banggai",
      "Banggai Kepulauan",
      "Buol",
      "Donggala",
      "Morowali",
      "Parigi Moutong",
      "Poso",
      "Tojo Una-Una",
      "Toli-Toli",
      "Sigi",
      "Palu"
    ],

    "Sulawesi Utara"=> [
      "Bolaang Mongondow",
      "Bolaang Mongondow Selatan",
      "Bolaang Mongondow Timur",
      "Bolaang Mongondow Utara",
      "Kepulauan Sangihe",
      "Kepulauan Siau Tagulandang Biaro",
      "Kepulauan Talaud",
      "Minahasa",
      "Minahasa Selatan",
      "Minahasa Tenggara",
      "Minahasa Utara",
      "Bitung",
      "Kotamobagu",
      "Manado",
      "Tomohon"
    ],

    "Sulawesi Barat"=> [
      "Majene",
      "Mamasa",
      "Mamuju",
      "Mamuju Utara",
      "Polewali Mandar"
    ],

    "Maluku"=> [
      "Buru",
      "Buru Selatan",
      "Kepulauan Aru",
      "Maluku Barat Daya",
      "Maluku Tengah",
      "Maluku Tenggara",
      "Maluku Tenggara Barat",
      "Seram Bagian Barat",
      "Seram Bagian Timur",
      "Ambon",
      "Tual"
    ],

    "Maluku Utara"=> [
      "Halmahera Barat",
      "Halmahera Tengah",
      "Halmahera Utara",
      "Halmahera Selatan",
      "Kepulauan Sula",
      "Halmahera Timur",
      "Pulau Morotai",
      "Ternate",
      "Tidore Kepulauan"
    ],

    "Papua"=> [
      "Asmat",
      "Biak Numfor",
      "Boven Digoel",
      "Deiyai",
      "Dogiyai",
      "Intan Jaya",
      "Jayapura",
      "Jayawijaya",
      "Keerom",
      "Kepulauan Yapen",
      "Lanny Jaya",
      "Mamberamo Raya",
      "Mamberamo Tengah",
      "Mappi",
      "Merauke",
      "Mimika",
      "Nabire",
      "Nduga",
      "Paniai",
      "Pegunungan Bintang",
      "Puncak",
      "Puncak Jaya",
      "Sarmi",
      "Supiori",
      "Tolikara",
      "Waropen",
      "Yahukimo",
      "Yalimo",
      "Jayapura"
    ],

    "Papua Barat"=> [
      "Fakfak",
      "Kaimana",
      "Manokwari",
      "Maybrat",
      "Raja Ampat",
      "Sorong",
      "Sorong Selatan",
      "Tambrauw",
      "Teluk Bintuni",
      "Teluk Wondama"
    ]
  }
  FEATURED_REGIONS = ["DKI Jakarta", "Bandung", "Surabaya", "Medan", "Semarang", "Yogyakarta", "Aceh", "Bali", "Manado"]
  LATLONG = [
              ["4.695135", "96.749399"], ["2.010856", "98.978489"], ["-3.792845", "102.260764"],
              ["-1.590000", "103.610000"], ["0.293347", "101.706829"], ["-0.739940", "100.800005"],
              ["-3.319437", "103.914399"], ["-4.558585", "105.406808"], ["-2.741051", "106.440587"],
              ["3.945651", "108.142867"], ["-6.405817", "106.064018"], ["-7.090911", "107.668887"],
              ["-6.174465", "106.822745"], ["-7.150975", "110.140259"], ["-7.536064", "112.238402"],
              ["-7.795580", "110.369490"], ["-8.409518", "115.188916"], ["-8.652933", "117.361648"],
              ["-8.657382", "121.079370"], ["-0.278781", "111.475285"], ["-3.092642", "115.283758"],
              ["-1.681488", "113.382355"], ["0.538659", "116.419389"], ["0.543544", "123.056769"],
              ["-3.668799", "119.974053"], ["-4.144910", "122.174605"], ["-1.430025", "121.445618"],
              ["0.624693", "123.975002"], ["-2.844137", "119.232078"], ["-3.238462", "130.145273"],
              ["1.570999", "127.808769"], ["-4.269928", "138.080353"], ["-1.336115", "133.174716"],
            ]
  def self.seed
    REGIONS.keys.each_with_index do |prov, i|
      province = Region.find_or_initialize_by(name: prov)
      province.name = prov
      province.latitude = LATLONG[i][0]
      province.longitude = LATLONG[i][1]
      province.save

      cities = REGIONS[province.name].first.class != String ? REGIONS[province.name].first.keys : REGIONS[province.name]
      cities.each do |cty|
        city = Region.find_or_initialize_by(name: cty)
        city.parent = province
        city.latitude = province.latitude.to_f + (rand 0.0001..0.0099)
        city.longitude = province.longitude.to_f + (rand 0.0001..0.0099)
        city.save
        sub_districts = REGIONS[province.name].first[city.name].class != String ? REGIONS[province.name].first[city.name] : []
        if sub_districts.present?
          sub_districts.each do |sub|
            dist = Region.find_or_initialize_by(name: sub)
            dist.parent = city
            dist.latitude = city.latitude.to_f + (rand 0.0001..0.0099)
            dist.longitude = city.longitude.to_f + (rand 0.0001..0.0099)
            dist.save
          end
        end
      end
    end

    FEATURED_REGIONS.each do |reg|
      city = Region.find_or_initialize_by(name: reg)
      city.featured = true
      city.save
    end
  end
end
