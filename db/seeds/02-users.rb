module SeedUser
	def self.seed
		user = User.find_or_initialize_by(email: "creativedesignjakarta@gmail.com")
		user.username = 'superadmin'
		user.password = 12345678
		user.password_confirmation = 12345678
		user.type = 'Admin'
		user.confirmation_token = nil
		user.confirmed_at = Time.now
		user.role_id = Role.first.id
		user.save
	end
end
