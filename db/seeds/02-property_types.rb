module SeedPropertyType
	TYPES = {
		"Dijual"=> [
			"Rumah",
			"Apartemen",
			"Ruko",
			"Tanah",
			"Perkantoran",
			"Villa"
		],

		"Disewa"=> [
			"Rumah",
			"Apartemen",
			"Ruko",
			"Perkantoran",
			"Villa"
		],

		"Digadai"=> [
			"Rumah",
			"Ruko",
			"Tanah",
			"Villa"
		],

		"Dilelang"=> [
			"Rumah",
			"Apartemen",
			"Ruko",
			"Tanah",
			"Villa"
		],
	}

	def self.seed
		TYPES.keys.each_with_index do |property_root, index|
			category = PropertyType.find_or_initialize_by(name: property_root)
			category.name = property_root
			category.save

			TYPES[category.name].each do |c|
				child = PropertyType.new
				child.parent = category
				child.name = c
				child.save
			end
		end
	end
end
