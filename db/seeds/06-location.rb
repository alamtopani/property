module SeedLocation
  def self.seed
    regions = Region.where("ancestry IS NULL")
    types = LocationType.to_a
    types.each do |type|
      regions.take(1).each do |reg|
        reg.children.take(1).each do |city|
          city.children.take(1).each do |sub|
            sub.children.take(1).each_with_index do |village, i|
              loc = Location.find_or_initialize_by({title: "#{type[0]}-#{city.name}"})
              loc.type_asset = type[1]
              loc.latitude = "#{get_rand_int(city.latitude)}"
              loc.longitude = "#{get_rand_int(city.longitude)}"
              add = loc.address
              add.address = "Jalan #{type[0]}-#{city.name} No. #{i}"
              add.city_id = city.id
              add.subdistrict_id = sub.id
              add.village_id = village.id
              add.province_id = reg.id
              add.postcode = "41#{i}12"
              loc.save
            end
            puts "created Location in #{city.name}"
          end
        end
      end
    end
  end

  def self.get_rand_int(int)
    int.to_f + (rand 0.0001..0.0099)
  end
end
