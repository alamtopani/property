module SeedRegion
  def self.seed
    ActiveRecord::Base.transaction do
      Region.delete_all

      CSV.foreach(File.join(Rails.root, "db", "seed_data", "01.provinsi.csv"), :headers => true, :encoding => 'UTF-8') do |row|
        row = row.to_hash
        if region = Region.find_or_create_by(row)
          puts "Created #{region.name}."
        else
          puts "COULD NOT CREATE #{row['name']}!"
        end
      end

      CSV.foreach(File.join(Rails.root, "db", "seed_data", "02.kabkota-clean.csv"), :headers => true, :encoding => 'UTF-8') do |row|
        row = row.to_hash
        if (region = Region.find_or_create_by(name: row['name'], ancestry: row['ancestry'], id: row['id']))
          region.update_attribute(:longitude, row['longitude'])
          region.update_attribute(:latitude, row['latitude'])
          puts "Created #{region.name} location for #{region.parent.name}."
        else
          puts "COULD NOT CREATE #{row['city']} location for #{row['region']}!"
        end
      end

      CSV.foreach(File.join(Rails.root, "db", "seed_data", "03.kecamatan-clean.csv"), :headers => true, :encoding => 'UTF-8') do |row|
        row = row.to_hash
        if (region = Region.find_or_create_by(row))
          parent_ancestry = region.parent.ancestry
          region.update_attribute(:ancestry, "#{parent_ancestry}/#{row['ancestry']}")
          puts "Created #{region.name} location for #{region.parent.name}."
        else
          puts "COULD NOT CREATE #{row['city']} location for #{row['region']}!"
        end
      end

      CSV.foreach(File.join(Rails.root, "db", "seed_data", "04.desa-clean.csv"), :headers => true, :encoding => 'UTF-8') do |row|
        row = row.to_hash
        if (region = Region.find_or_create_by(row))
          parent_ancestry = region.parent.ancestry
          region.update_attribute(:ancestry, "#{parent_ancestry}/#{row['ancestry']}")
          puts "Created #{region.name} location for #{region.parent.name}."
        else
          puts "COULD NOT CREATE #{row['city']} location for #{row['region']}!"
        end
      end

    end
    ActiveRecord::Base.connection.execute("SELECT setval('regions_id_seq', 534) FROM regions;")
    # feature_regions = ["DKI Jakarta", "Kota Bandung", "Kota Surabaya", "Kota Medan", "Kota Semarang", "Kota Yogyakarta", "Aceh", "Bali", "Kota Manado"]
    # feature_regions.each do |reg|
    #   city = Region.find_or_initialize_by(name: reg)
    #   city.featured = true
    #   city.save
    # end
  end
end
