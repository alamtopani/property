module SeedLandingPage
  def self.seed
    landing_pages = [
      {
        title: 'About Us',
        description: 'Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet.',
        status: true,
        category: 'company',
      },
      {
        title: 'Testimonial',
        description: 'Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet.',
        status: true,
        category: 'company',
      },
      {
        title: 'Terms',
        description: 'Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet.',
        status: true,
        category: 'company',
      },
      {
        title: 'Privacy Policy',
        description: 'Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet.',
        status: true,
        category: 'company',
      },
      {
        title: 'Contact Us',
        description: 'Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet.',
        status: true,
        category: 'company',
      },
      {
        title: 'Careers',
        description: 'Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet.',
        status: true,
        category: 'company',
      },
      {
        title: 'Blog',
        description: 'Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet.',
        status: true,
        category: 'explore',
      },
      {
        title: 'Sitemap',
        description: 'Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet.',
        status: true,
        category: 'explore',
      },
      {
        title: 'Facebook',
        description: 'Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet.',
        status: true,
        category: 'follow',
      },
      {
        title: 'Twitter',
        description: 'Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet.',
        status: true,
        category: 'follow',
      },
      {
        title: 'Google+',
        description: 'Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet.',
        status: true,
        category: 'follow',
      },
      {
        title: 'Linkedin',
        description: 'Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet.',
        status: true,
        category: 'follow'
      }
    ]

    landing_pages.each do |f|
      pages = {title: f[:title], description: f[:description], status: f[:status], category: f[:category]}
      page  = LandingPage.where(pages).first || LandingPage.new(pages)
      page.save
    end
  end
end
