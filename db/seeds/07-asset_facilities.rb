module SeedAssetFacility
	ASSETS = [
		['AC', '<i class="fa fa-asterisk"></i>','["Rumah","Apartemen","Ruko","Perkantoran","Villa"]'],
        ['TV', '<i class="fa fa-desktop"></i>','["Rumah","Apartemen","Ruko"]'],
        ['Kursi', '<i class="fa fa-wheelchair"></i>','["Rumah","Apartemen"]'],
        ['Meja Makan', '<i class="fa fa-square"></i>','["Rumah","Apartemen"]'],
        ['Microwave', '<i class="fa fa-toggle-on"></i>','["Rumah","Apartemen"]'],
        ['Kulkas', '<i class="fa fa-columns"></i>','["Rumah","Apartemen"]'],
        ['Kompor', '<i class="fa fa-fire"></i>','["Rumah","Apartemen"]'],
        ['Mesin Cuci', '<i class="fa fa-refresh"></i>','["Rumah","Apartemen"]'],
        ['Pelayanan Ruangan', '<i class="fa fa-child"></i>','["Rumah","Apartemen","Ruko","Perkantoran","Villa"]'],
        ['Lemari', '<i class="fa fa-th-list"></i>','["Rumah","Apartemen","Perkantoran","Villa"]'],
        ['Dapur','<i class="fa fa-filter"></i>','["Rumah","Apartemen","Perkantoran"]'],
        ['Air Panas','<i class="fa fa-yelp"></i>','["Rumah","Apartemen"]'],
        ['Pemurni Air','<i class="fa fa-bookmark"></i>','["Rumah","Apartemen","Ruko"]'],
        ['Internet','<i class="fa fa-random"></i>','["Rumah","Apartemen","Ruko","Perkantoran","Villa"]'],
        ['Wifi','<i class="fa fa-wifi"></i>','["Rumah","Apartemen","Ruko","Perkantoran","Villa"]'],
        ['Keamanan','<i class="fa fa-shield"></i>','["Rumah","Apartemen","Ruko","Perkantoran","Villa"]'],

        ['Gym', '<i class="fa fa-chain"></i>','["Apartemen","Perkantoran","Villa"]'],
        ['Kolam Renang', '<i class="fa fa-life-saver"></i>','["Rumah","Apartemen","Perkantoran","Villa"]'],
        ['Pipa Gas', '<i class="fa fa-sliders"></i>','["Rumah","Apartemen","Ruko","Villa","Perkantoran"]'],
        ['Power Backup', '<i class="fa fa-lightbulb-o"></i>','["Rumah","Apartemen","Ruko","Perkantoran","Villa"]'],
        ['Persediaan Air', '<i class="fa fa-tint"></i>','["Rumah","Apartemen","Ruko","Perkantoran","Villa"]'],
        ['Keamanan masyarakat', '<i class="fa fa-inbox"></i>','["Rumah","Apartemen","Ruko","Perkantoran","Villa"]'],
        ['Taman', '<i class="fa fa-tree"></i>','["Rumah","Apartemen","Perkantoran","Villa"]'],
        ['Arena bermain anak', '<i class="fa fa-smile-o"></i>','["Rumah","Apartemen","Perkantoran"]'],
        ['Fasiltas Olahraga', '<i class="fa fa-street-view"></i>','["Rumah","Apartemen","Ruko","Perkantoran","Villa"]'],
        ['Lift', '<i class="fa fa-arrows-v"></i>','["Apartemen","Perkantoran","Villa"]'],
        ['Parkir','<i class="fa fa-car"></i>','["Rumah","Apartemen","Ruko","Perkantoran","Villa"]']
	]

	def self.seed
		ASSETS.each do |asset|
			facility = AssetFacility.find_or_initialize_by(name: asset[0])
			facility.font_icon = asset[1]
			facility.category = asset[2]
			facility.save
		end
	end
end
