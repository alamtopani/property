module SeedProperty
  def self.seed
    # update_current_package_user
    # regions = Region.where("ancestry IS NULL").where(name: 'DKI Jakarta')
    # types = PropertyType.pluck(:name)
    # (1..1000).each do |time|
    #   types.each do |type|
    #     regions.each do |reg|
    #       reg.children.where(name: 'Kota Jakarta Pusat').each do |city|
    #         city.children.where(name: 'Gambir').each do |sub|
    #           sub.children.where(name: 'Gambir').each_with_index do |village, i|
    #             prop = Property.find_or_initialize_by({code: "#{village.name}-#{type}-#{time}"})
    #             prop.title = "#{village.name} #{type}"
    #             prop.type_property = "#{type}"
    #             prop.user_id = Member.first.id
    #             add = prop.address
    #             add.latitude = "#{get_rand_int(city.latitude, time)}"
    #             add.longitude = "#{get_rand_int(city.longitude, time)}"
    #             add.address = "Jalan #{type}-#{village.name} No. #{i}"
    #             add.city_id = city.id
    #             add.subdistrict_id = sub.id
    #             add.village_id = village.id
    #             add.province_id = reg.id
    #             prop.save
    #           end
    #           puts "created property in #{city.name}"
    #         end
    #       end
    #     end
    #   end
    # end
  end

  def self.get_rand_int(int, time)
    int.to_f + (rand 0.0001..0.0099) + (time.to_f/1000)
  end

  def self.update_current_package_user
    current_package_user = Member.first.current_package
    current_package_user.max_listing = 1000000
    current_package_user.featured_listing = 1000000
    current_package_user.expire_in = Date.tomorrow
    current_package_user.save
  end
end
