class CreateSocieties < ActiveRecord::Migration
  def change
    create_table :societies do |t|
      t.string :title
      t.boolean :available
      t.string :icon
      t.string :societiable_type
      t.integer :societiable_id
      t.integer :position

      t.timestamps null: false
    end
  end
end
