class AddPackageNameToUserPackages < ActiveRecord::Migration
  def change
    add_column :user_packages, :package_name, :string
  end
end
