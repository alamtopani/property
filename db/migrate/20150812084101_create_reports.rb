class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.string :category
      t.string :name
      t.string :email
      t.text :message
      t.boolean :status, default: false
      t.integer :reportable_id
      t.string :reportable_type

      t.timestamps null: false
    end
  end
end
