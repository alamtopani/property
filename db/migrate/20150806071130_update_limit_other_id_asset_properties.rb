class UpdateLimitOtherIdAssetProperties < ActiveRecord::Migration
  def change
    change_column :asset_properties, :location_id, :integer, limit: 8
    change_column :asset_properties, :property_id, :integer, limit: 8
  end
end
