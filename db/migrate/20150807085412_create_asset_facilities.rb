class CreateAssetFacilities < ActiveRecord::Migration
  def change
    create_table :asset_facilities do |t|
      t.string :name
      t.string :font_icon
      t.string :category
      t.timestamps null: false
    end
  end
end
