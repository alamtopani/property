class AddColumnCodeToUserPackages < ActiveRecord::Migration
  def change
    add_column :user_packages, :code, :string
  end
end
