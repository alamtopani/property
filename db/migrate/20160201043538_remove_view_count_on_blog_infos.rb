class RemoveViewCountOnBlogInfos < ActiveRecord::Migration
  def change
    remove_column :blog_infos, :view_count
  end
end
