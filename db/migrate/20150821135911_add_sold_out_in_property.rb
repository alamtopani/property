class AddSoldOutInProperty < ActiveRecord::Migration
  def change
  	add_column :properties, :sold_out, :boolean, default: false
  end
end
