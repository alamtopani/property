class CreateTestimonials < ActiveRecord::Migration
  def change
    create_table :testimonials do |t|
      t.string :name
      t.string :email
      t.string :position
      t.integer :user_id
      t.text :message
      t.boolean :activated

      t.timestamps null: false
    end
  end
end
