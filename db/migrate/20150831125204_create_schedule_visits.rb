class CreateScheduleVisits < ActiveRecord::Migration
  def change
    create_table :schedule_visits do |t|
      t.string :first_name
      t.string :last_name
      t.datetime :schedule_date
      t.datetime :alt_schedule_date
      t.string :phone
      t.string :email

      t.timestamps null: false
    end
  end
end
