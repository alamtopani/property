class AddPriceToUserPackages < ActiveRecord::Migration
  def change
    add_column :user_packages, :price, :decimal
  end
end