class CreateCurrentPackageUsers < ActiveRecord::Migration
  def change
    create_table :current_package_users do |t|
      t.integer :user_id
      t.date :expire_in
      t.boolean :top_listing
      t.integer :max_listing
      t.integer :featured_listing
      t.string :label_tags

      t.timestamps null: false
    end
  end
end
