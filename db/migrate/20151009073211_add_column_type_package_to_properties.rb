class AddColumnTypePackageToProperties < ActiveRecord::Migration
  def change
    add_column :properties, :type_package, :string
  end
end
