class CreateInteriors < ActiveRecord::Migration
  def change
    create_table :interiors do |t|
      t.string :title
      t.string :val
      t.integer :position
      t.string :interiorable_type
      t.integer :interiorable_id
      t.string :category

      t.timestamps null: false
    end
  end
end
