class UpdateLimitIdRegions < ActiveRecord::Migration
  def change
    change_column :regions, :id, :integer, limit: 8
  end
end
