class CreateAssetProperties < ActiveRecord::Migration
  def change
    create_table :asset_properties do |t|
      t.integer :location_id
      t.integer :property_id
      t.string :distance
      t.string :duration

      t.timestamps null: false
    end
  end
end
