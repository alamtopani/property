class AddVerifiedInLocation < ActiveRecord::Migration
  def change
  	add_column :locations, :verified, :boolean, default: false
  end
end
