class CreateAssets < ActiveRecord::Migration
  def change
    create_table :assets do |t|
      t.string :title
      t.string :type_asset
      t.string :latitude
      t.string :longitude
      t.integer :user_id

      t.timestamps null: false
    end
    add_index :assets, :user_id

  end
end
