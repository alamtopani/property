class UpdateLimitIdOthersTables < ActiveRecord::Migration
  def change
    change_column :galleries, :id, :integer, limit: 8
    change_column :amenities, :id, :integer, limit: 8
    change_column :asset_properties, :id, :integer, limit: 8
    change_column :basic_properties, :id, :integer, limit: 8
    change_column :inquiries, :id, :integer, limit: 8
    change_column :interiors, :id, :integer, limit: 8
    change_column :users, :id, :integer, limit: 8
    change_column :profiles, :id, :integer, limit: 8
    change_column :societies, :id, :integer, limit: 8
  end
end
