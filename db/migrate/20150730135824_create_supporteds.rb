class CreateSupporteds < ActiveRecord::Migration
  def change
    create_table :supporteds do |t|
      t.string :title
      t.string :link
      t.attachment :logo

      t.timestamps null: false
    end
  end
end
