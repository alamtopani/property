class AddColumnLabelTagToProperties < ActiveRecord::Migration
  def change
    add_column :properties, :label_tag, :string
  end
end
