class AddAtributMetaSeoOnWebSetting < ActiveRecord::Migration
  def change
    add_column :web_settings, :robot, :string
    add_column :web_settings, :author, :string
    add_column :web_settings, :corpyright, :string
    add_column :web_settings, :revisit, :string
    add_column :web_settings, :expires, :string
    add_column :web_settings, :revisit_after, :string
    add_column :web_settings, :geo_placename, :string
  end
end
