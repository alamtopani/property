class AddAreaInProject < ActiveRecord::Migration
  def change
    add_column :projects, :area, :integer
  end
end
