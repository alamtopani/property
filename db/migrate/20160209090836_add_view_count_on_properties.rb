class AddViewCountOnProperties < ActiveRecord::Migration
  def change
    add_column :properties, :view_count, :integer, default: 0
  end
end
