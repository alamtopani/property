class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.string :title
      t.string :slug
      t.string :code
      t.text :description
      t.string :status
      t.boolean :activated
      t.boolean :featured
      t.string :project_size
      t.string :construction
      t.string :type_project
      t.integer :user_id
      t.date :started_at
      t.date :expired_at
      t.decimal :price
      t.date :possession_start

      t.timestamps null: false
    end

    add_index :projects, :user_id
  end
end
