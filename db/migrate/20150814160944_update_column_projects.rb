class UpdateColumnProjects < ActiveRecord::Migration
  def change
    change_column :projects, :project_size, 'integer USING CAST(project_size AS integer)', default: 0
    change_column :projects, :price, :decimal, default: 0
  end
end
