class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.string :full_name
      t.date :birthday
      t.string :gender
      t.integer :user_id
      t.attachment :avatar
      t.string :meta_title
      t.string :meta_keyword
      t.text :meta_description

      t.timestamps null: false
    end

    add_index :profiles, :user_id
  end
end
