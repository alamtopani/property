class UpdateBasicPropertiesFieldTypeStringToInteger < ActiveRecord::Migration
  def change
    BasicProperty.update_all(surface_area: 0, building_area: 0, floor: 0, bathrooms: 0, bedrooms: 0)
    change_column :basic_properties, :surface_area, 'integer USING CAST(surface_area AS integer)'
    change_column :basic_properties, :building_area, 'integer USING CAST(building_area AS integer)'
    change_column :basic_properties, :floor, 'integer USING CAST(floor AS integer)'
    change_column :basic_properties, :bathrooms, 'integer USING CAST(bathrooms AS integer)'
    change_column :basic_properties, :bedrooms, 'integer USING CAST(bedrooms AS integer)'
  end
end
