class AddColumnAncestryDepthToPropertyTypes < ActiveRecord::Migration
  def change
    add_column :property_types, :ancestry_depth, :integer, default: 0
  end
end
