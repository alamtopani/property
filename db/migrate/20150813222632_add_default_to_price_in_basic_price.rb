class AddDefaultToPriceInBasicPrice < ActiveRecord::Migration
  def change
    change_column :basic_properties, :price_in_meter, :decimal, default: 0
    change_column :basic_properties, :phone_lines, :integer, default: 0
    change_column :basic_properties, :garage, :integer, default: 0
    change_column :basic_properties, :price_security, :decimal, default: 0
    change_column :basic_properties, :price_day, :decimal, default: 0
    change_column :basic_properties, :price_month, :decimal, default: 0
    change_column :basic_properties, :price_year, :decimal, default: 0
  end
end
