class AddCompanyWebsiteInAgentInfo < ActiveRecord::Migration
  def change
    add_column :agent_infos, :company_web, :string
  end
end
