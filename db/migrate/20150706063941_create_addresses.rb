class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.text :address
      t.string :city
      t.string :province
      t.string :postcode
      t.string :latitude
      t.string :longitude
      t.string :addressable_type
      t.integer :addressable_id

      t.timestamps null: false
    end
  end
end
