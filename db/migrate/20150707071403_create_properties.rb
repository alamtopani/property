class CreateProperties < ActiveRecord::Migration
  def change
    create_table :properties do |t|
      t.string :slug
      t.string :code
      t.string :title
      t.text :description
      t.decimal :price
      t.string :status
      t.boolean :activated, default: false
      t.boolean :featured, default: false
      t.integer :user_id
      t.string :type_property
      t.string :category
      t.date :started_at
      t.date :expired_at

      t.timestamps null: false
    end

    add_index :properties, :user_id
  end
end
