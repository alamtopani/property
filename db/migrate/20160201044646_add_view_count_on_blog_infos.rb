class AddViewCountOnBlogInfos < ActiveRecord::Migration
  def change
    add_column :blog_infos, :view_count, :integer, default: 0
  end
end
