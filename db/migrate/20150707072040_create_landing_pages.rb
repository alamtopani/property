class CreateLandingPages < ActiveRecord::Migration
  def change
    create_table :landing_pages do |t|
      t.string :title
      t.string :slug
      t.text :description
      t.boolean :status
      t.string :category

      t.timestamps null: false
    end
  end
end
