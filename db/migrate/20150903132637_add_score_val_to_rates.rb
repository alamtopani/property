class AddScoreValToRates < ActiveRecord::Migration
  def change
    add_column :rates, :score, :float, :null => false
  end
end
