class AddDescriptionInRegion < ActiveRecord::Migration
  def change
    add_column :regions, :description, :text
    add_column :regions, :link_url, :string
  end
end
