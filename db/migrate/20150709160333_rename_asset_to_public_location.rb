class RenameAssetToPublicLocation < ActiveRecord::Migration
  def change
  	rename_table :assets, :locations
  end
end
