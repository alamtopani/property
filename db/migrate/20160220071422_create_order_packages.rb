class CreateOrderPackages < ActiveRecord::Migration
  def change
    create_table :order_packages do |t|
      t.integer :user_id
      t.integer :price
      t.string :package_name
      t.boolean :status
      t.integer :staf_id
      t.string :code

      t.timestamps null: false
    end

    add_index :order_packages, :user_id
    add_index :order_packages, :staf_id
  end
end
