class AddColumnToUserPackages < ActiveRecord::Migration
  def change
    add_column :user_packages, :top_listing, :boolean, default: false
    add_column :user_packages, :max_listing, :integer
    add_column :user_packages, :featured_listing, :integer
  end
end
