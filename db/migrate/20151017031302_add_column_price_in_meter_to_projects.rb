class AddColumnPriceInMeterToProjects < ActiveRecord::Migration
  def change
    add_column :projects, :price_in_meter, :decimal
  end
end
