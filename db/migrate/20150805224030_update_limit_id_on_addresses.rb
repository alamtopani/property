class UpdateLimitIdOnAddresses < ActiveRecord::Migration
  def change
    change_column :addresses, :city_id, :integer, limit: 8
    change_column :addresses, :province_id, :integer, limit: 8
    change_column :addresses, :subdistrict_id, :integer, limit: 8
    change_column :addresses, :village_id, :integer, limit: 8
  end
end
