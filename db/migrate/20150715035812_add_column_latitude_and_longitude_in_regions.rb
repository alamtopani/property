class AddColumnLatitudeAndLongitudeInRegions < ActiveRecord::Migration
  def change
    add_column :regions, :latitude, :string
    add_column :regions, :longitude, :string
  end
end
