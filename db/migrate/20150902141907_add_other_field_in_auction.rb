class AddOtherFieldInAuction < ActiveRecord::Migration
  def change
  	add_column :auctions, :minimum_price, :decimal
  	add_column :auctions, :increment_per_bid, :decimal
  	add_column :auctions, :starting_bid, :decimal
  end
end
