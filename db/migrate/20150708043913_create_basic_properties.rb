class CreateBasicProperties < ActiveRecord::Migration
  def change
    create_table :basic_properties do |t|
      t.integer :property_id
      t.string :bathrooms
      t.string :bedrooms
      t.string :parking
      t.string :facing
      t.string :floor
      t.string :age
      t.string :built_up_area

      t.timestamps null: false
    end

    add_index :basic_properties, :property_id
  end
end
