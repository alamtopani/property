class CreateUserPackages < ActiveRecord::Migration
  def change
    create_table :user_packages do |t|
      t.references :user
      t.references :package
      t.date :expire_in
      t.boolean :payed

      t.timestamps null: false
    end
  end
end
