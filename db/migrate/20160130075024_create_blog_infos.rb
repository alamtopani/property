class CreateBlogInfos < ActiveRecord::Migration
  def change
    create_table :blog_infos do |t|
      t.string :title
      t.string :slug
      t.text :description
      t.integer :category_id
      t.integer :user_id
      t.attachment :cover
      t.boolean :status
      t.boolean :featured
      t.integer :view_count, default: 0
      t.string :source

      t.timestamps null: false
    end
  end
end
