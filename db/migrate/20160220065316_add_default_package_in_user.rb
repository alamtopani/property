class AddDefaultPackageInUser < ActiveRecord::Migration
  def change
    add_column :users, :limit, :integer, default: 10
    add_column :users, :expired_at, :date
    add_column :users, :package, :string, default: 'free'
  end
end
