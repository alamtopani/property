class ChangeColumnDefaultPackageAndUserPackage < ActiveRecord::Migration
  def change
    change_column :packages, :active, :boolean, default: false
    change_column :user_packages, :payed, :boolean, default: false
  end
end
