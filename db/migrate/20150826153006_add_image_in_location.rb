class AddImageInLocation < ActiveRecord::Migration
  def self.up
    change_table :regions do |t|
      t.attachment :image
    end
  end

  def self.down
    drop_attached_file :regions, :image
  end
end
