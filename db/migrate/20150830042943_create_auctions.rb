class CreateAuctions < ActiveRecord::Migration
  def change
    create_table :auctions do |t|
      t.integer :property_id
      t.datetime :start_auction
      t.datetime :end_auction

      t.timestamps null: false
    end

    add_index :auctions, :property_id
  end
end
