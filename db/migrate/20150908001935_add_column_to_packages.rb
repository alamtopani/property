class AddColumnToPackages < ActiveRecord::Migration
  def change
    add_column :packages, :gratis_bulan, :integer, default: 1
    add_column :packages, :top_listing, :boolean, default: false
    add_column :packages, :max_listing, :integer
    add_column :packages, :featured_listing, :integer
    add_column :packages, :price_year, :decimal
  end
end
