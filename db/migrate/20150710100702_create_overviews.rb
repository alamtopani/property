class CreateOverviews < ActiveRecord::Migration
  def change
    create_table :overviews do |t|
      t.string :title
      t.attachment :file_2d
      t.attachment :file_3d
      t.date :possession_start
      t.string :saleable_area
      t.decimal :price
      t.string :overviewable_type
      t.integer :overviewable_id
      t.integer :position
      t.integer :toilets
      t.integer :balconies

      t.timestamps null: false
    end
  end
end
