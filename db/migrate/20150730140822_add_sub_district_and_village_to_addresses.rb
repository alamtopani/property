class AddSubDistrictAndVillageToAddresses < ActiveRecord::Migration
  def change
    add_column :addresses, :sub_district, :string
    add_column :addresses, :village, :string
  end
end
