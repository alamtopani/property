class AddKtpInAgentInfo < ActiveRecord::Migration
  def self.up
    change_table :agent_infos do |t|
      t.attachment :ktp
    end
  end

  def self.down
    drop_attached_file :agent_infos, :ktp
  end
end
