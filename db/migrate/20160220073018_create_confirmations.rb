class CreateConfirmations < ActiveRecord::Migration
  def change
    create_table :confirmations do |t|
      t.integer :user_id
      t.string :slug
      t.string :code
      t.string :no_invoice
      t.string :name
      t.string :email
      t.date :payment_date
      t.integer :nominal
      t.string :bank_account
      t.string :payment_method
      t.string :sender_name
      t.text :message
      t.string :category

      t.timestamps null: false
    end

    add_index :confirmations, :user_id
  end
end
