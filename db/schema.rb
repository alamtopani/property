# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160223050726) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "addresses", id: :bigserial, force: :cascade do |t|
    t.text     "address"
    t.string   "city"
    t.string   "province"
    t.string   "postcode"
    t.string   "latitude"
    t.string   "longitude"
    t.string   "addressable_type"
    t.integer  "addressable_id"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.string   "sub_district"
    t.string   "village"
    t.integer  "province_id",      limit: 8
    t.integer  "city_id",          limit: 8
    t.integer  "subdistrict_id",   limit: 8
    t.integer  "village_id",       limit: 8
    t.string   "area"
    t.integer  "area_id",          limit: 8, default: 0
  end

  create_table "agent_infos", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "specialist_property"
    t.string   "specialist_area"
    t.text     "description"
    t.string   "language"
    t.string   "name_company"
    t.string   "company_logo_file_name"
    t.string   "company_logo_content_type"
    t.integer  "company_logo_file_size"
    t.datetime "company_logo_updated_at"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "company_web"
    t.string   "ktp_file_name"
    t.string   "ktp_content_type"
    t.integer  "ktp_file_size"
    t.datetime "ktp_updated_at"
  end

  add_index "agent_infos", ["user_id"], name: "index_agent_infos_on_user_id", using: :btree

  create_table "ahoy_events", id: :uuid, default: nil, force: :cascade do |t|
    t.uuid     "visit_id"
    t.integer  "user_id"
    t.string   "name"
    t.json     "properties"
    t.datetime "time"
  end

  add_index "ahoy_events", ["time"], name: "index_ahoy_events_on_time", using: :btree
  add_index "ahoy_events", ["user_id"], name: "index_ahoy_events_on_user_id", using: :btree
  add_index "ahoy_events", ["visit_id"], name: "index_ahoy_events_on_visit_id", using: :btree

  create_table "amenities", id: :bigserial, force: :cascade do |t|
    t.string   "title"
    t.boolean  "available"
    t.string   "amenitiable_type"
    t.integer  "amenitiable_id"
    t.string   "icon"
    t.integer  "position"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "asset_facilities", force: :cascade do |t|
    t.string   "name"
    t.string   "font_icon"
    t.string   "category"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "asset_projects", force: :cascade do |t|
    t.integer  "project_id"
    t.integer  "location_id"
    t.string   "distance"
    t.string   "duration"
    t.string   "type_asset"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "asset_properties", id: :bigserial, force: :cascade do |t|
    t.integer  "location_id", limit: 8
    t.integer  "property_id", limit: 8
    t.string   "distance"
    t.string   "duration"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.string   "type_asset"
  end

  create_table "auctions", force: :cascade do |t|
    t.integer  "property_id"
    t.datetime "start_auction"
    t.datetime "end_auction"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.string   "reserve_met"
    t.string   "financing_considered"
    t.decimal  "minimum_price"
    t.decimal  "increment_per_bid"
    t.decimal  "starting_bid"
  end

  add_index "auctions", ["property_id"], name: "index_auctions_on_property_id", using: :btree

  create_table "average_caches", force: :cascade do |t|
    t.integer  "rater_id"
    t.integer  "rateable_id"
    t.string   "rateable_type"
    t.float    "avg",           null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "basic_properties", id: :bigserial, force: :cascade do |t|
    t.integer  "property_id"
    t.integer  "bathrooms"
    t.integer  "bedrooms"
    t.string   "parking"
    t.string   "facing"
    t.integer  "floor"
    t.string   "age"
    t.string   "built_up_area"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.string   "document"
    t.integer  "surface_area"
    t.integer  "building_area"
    t.string   "electricity"
    t.integer  "phone_lines",    default: 0
    t.integer  "garage",         default: 0
    t.decimal  "price_in_meter", default: 0.0
    t.string   "business_unit"
    t.decimal  "price_security", default: 0.0
    t.decimal  "price_day",      default: 0.0
    t.decimal  "price_month",    default: 0.0
    t.decimal  "price_year",     default: 0.0
    t.string   "width_land"
    t.string   "long_land"
  end

  add_index "basic_properties", ["property_id"], name: "index_basic_properties_on_property_id", using: :btree

  create_table "bid_auctions", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "property_id"
    t.float    "bid"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "bid_auctions", ["property_id"], name: "index_bid_auctions_on_property_id", using: :btree
  add_index "bid_auctions", ["user_id"], name: "index_bid_auctions_on_user_id", using: :btree

  create_table "blog_infos", force: :cascade do |t|
    t.string   "title"
    t.string   "slug"
    t.text     "description"
    t.integer  "category_id"
    t.integer  "user_id"
    t.string   "cover_file_name"
    t.string   "cover_content_type"
    t.integer  "cover_file_size"
    t.datetime "cover_updated_at"
    t.boolean  "status"
    t.boolean  "featured"
    t.string   "source"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "view_count",         default: 0
  end

  create_table "category_blog_infos", force: :cascade do |t|
    t.string   "slug"
    t.string   "name"
    t.string   "ancestry"
    t.text     "description"
    t.string   "cover_file_name"
    t.string   "cover_content_type"
    t.integer  "cover_file_size"
    t.datetime "cover_updated_at"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "ckeditor_assets", force: :cascade do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable", using: :btree
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type", using: :btree

  create_table "confirmations", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "slug"
    t.string   "code"
    t.string   "no_invoice"
    t.string   "name"
    t.string   "email"
    t.date     "payment_date"
    t.integer  "nominal"
    t.string   "bank_account"
    t.string   "payment_method"
    t.string   "sender_name"
    t.text     "message"
    t.string   "category"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "confirmations", ["user_id"], name: "index_confirmations_on_user_id", using: :btree

  create_table "contacts", force: :cascade do |t|
    t.string   "phone"
    t.string   "handphone"
    t.string   "pin"
    t.string   "facebook"
    t.string   "twitter"
    t.string   "contactable_type"
    t.integer  "contactable_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "website"
  end

  create_table "current_package_users", force: :cascade do |t|
    t.integer  "user_id"
    t.date     "expire_in"
    t.boolean  "top_listing"
    t.integer  "max_listing"
    t.integer  "featured_listing"
    t.string   "label_tags"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "custom_auto_increments", force: :cascade do |t|
    t.string   "model_name"
    t.integer  "counter",    default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "custom_auto_increments", ["model_name"], name: "index_custom_auto_increments_on_model_name", using: :btree

  create_table "feedbacks", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "category"
    t.text     "message"
    t.string   "phone"
    t.boolean  "status",     default: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.string   "code"
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "galleries", id: :bigserial, force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
    t.string   "galleriable_type"
    t.integer  "galleriable_id"
    t.integer  "position"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  create_table "impressions", force: :cascade do |t|
    t.string   "impressionable_type"
    t.integer  "impressionable_id"
    t.integer  "user_id"
    t.string   "controller_name"
    t.string   "action_name"
    t.string   "view_name"
    t.string   "request_hash"
    t.string   "ip_address"
    t.string   "session_hash"
    t.text     "message"
    t.text     "referrer"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "counter_cache"
  end

  add_index "impressions", ["controller_name", "action_name", "ip_address"], name: "controlleraction_ip_index", using: :btree
  add_index "impressions", ["controller_name", "action_name", "request_hash"], name: "controlleraction_request_index", using: :btree
  add_index "impressions", ["controller_name", "action_name", "session_hash"], name: "controlleraction_session_index", using: :btree
  add_index "impressions", ["impressionable_type", "impressionable_id", "ip_address"], name: "poly_ip_index", using: :btree
  add_index "impressions", ["impressionable_type", "impressionable_id", "request_hash"], name: "poly_request_index", using: :btree
  add_index "impressions", ["impressionable_type", "impressionable_id", "session_hash"], name: "poly_session_index", using: :btree
  add_index "impressions", ["impressionable_type", "message", "impressionable_id"], name: "impressionable_type_message_index", using: :btree
  add_index "impressions", ["user_id"], name: "index_impressions_on_user_id", using: :btree

  create_table "inquiries", id: :bigserial, force: :cascade do |t|
    t.integer  "user_id"
    t.string   "name"
    t.string   "email"
    t.string   "phone"
    t.text     "message"
    t.string   "inquiriable_type"
    t.integer  "inquiriable_id"
    t.boolean  "status",           default: false
    t.string   "ancestry"
    t.integer  "owner_id"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.string   "subject"
  end

  add_index "inquiries", ["owner_id"], name: "index_inquiries_on_owner_id", using: :btree
  add_index "inquiries", ["user_id"], name: "index_inquiries_on_user_id", using: :btree

  create_table "interiors", id: :bigserial, force: :cascade do |t|
    t.string   "title"
    t.string   "val"
    t.integer  "position"
    t.string   "interiorable_type"
    t.integer  "interiorable_id"
    t.string   "category"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  create_table "landing_pages", force: :cascade do |t|
    t.string   "title"
    t.string   "slug"
    t.text     "description"
    t.boolean  "status"
    t.string   "category"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.text     "link_url"
  end

  create_table "locations", id: :bigserial, force: :cascade do |t|
    t.string   "title"
    t.string   "type_asset"
    t.string   "latitude"
    t.string   "longitude"
    t.integer  "user_id"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.boolean  "verified",   default: false
  end

  add_index "locations", ["user_id"], name: "index_locations_on_user_id", using: :btree

  create_table "order_packages", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "price"
    t.string   "package_name"
    t.boolean  "status"
    t.integer  "staf_id"
    t.string   "code"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "order_packages", ["staf_id"], name: "index_order_packages_on_staf_id", using: :btree
  add_index "order_packages", ["user_id"], name: "index_order_packages_on_user_id", using: :btree

  create_table "overall_averages", force: :cascade do |t|
    t.integer  "rateable_id"
    t.string   "rateable_type"
    t.float    "overall_avg",   null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "overviews", force: :cascade do |t|
    t.string   "title"
    t.string   "file_2d_file_name"
    t.string   "file_2d_content_type"
    t.integer  "file_2d_file_size"
    t.datetime "file_2d_updated_at"
    t.string   "file_3d_file_name"
    t.string   "file_3d_content_type"
    t.integer  "file_3d_file_size"
    t.datetime "file_3d_updated_at"
    t.date     "possession_start"
    t.string   "saleable_area"
    t.decimal  "price"
    t.string   "overviewable_type"
    t.integer  "overviewable_id"
    t.integer  "position"
    t.integer  "toilets"
    t.integer  "balconies"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "packages", force: :cascade do |t|
    t.string   "name"
    t.integer  "duration"
    t.integer  "description"
    t.decimal  "price"
    t.boolean  "active",           default: false
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.integer  "gratis_bulan",     default: 1
    t.boolean  "top_listing",      default: false
    t.integer  "max_listing"
    t.integer  "featured_listing"
    t.decimal  "price_year"
    t.string   "label_tags"
  end

  create_table "payments", force: :cascade do |t|
    t.string   "name"
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
    t.string   "bank_name"
    t.string   "account_number"
    t.integer  "paymentable_id"
    t.string   "paymentable_type"
    t.integer  "position"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  create_table "profiles", id: :bigserial, force: :cascade do |t|
    t.string   "full_name"
    t.date     "birthday"
    t.string   "gender"
    t.integer  "user_id"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "meta_title"
    t.string   "meta_keyword"
    t.text     "meta_description"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.string   "cover_file_name"
    t.string   "cover_content_type"
    t.integer  "cover_file_size"
    t.datetime "cover_updated_at"
  end

  add_index "profiles", ["user_id"], name: "index_profiles_on_user_id", using: :btree

  create_table "projects", id: :bigserial, force: :cascade do |t|
    t.string   "title"
    t.string   "slug"
    t.string   "code"
    t.text     "description"
    t.string   "status"
    t.boolean  "activated"
    t.boolean  "featured"
    t.integer  "project_size",     default: 0
    t.string   "construction"
    t.string   "type_project"
    t.integer  "user_id"
    t.date     "started_at"
    t.date     "expired_at"
    t.decimal  "price",            default: 0.0
    t.date     "possession_start"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.text     "insights"
    t.string   "facilities",                                  array: true
    t.decimal  "price_in_meter"
    t.integer  "area"
  end

  add_index "projects", ["user_id"], name: "index_projects_on_user_id", using: :btree

  create_table "properties", id: :bigserial, force: :cascade do |t|
    t.string   "slug"
    t.string   "code"
    t.string   "title"
    t.text     "description"
    t.decimal  "price"
    t.string   "status"
    t.boolean  "activated",     default: false
    t.boolean  "featured",      default: false
    t.integer  "user_id"
    t.string   "type_property"
    t.string   "category"
    t.date     "started_at"
    t.date     "expired_at"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.boolean  "negotiable"
    t.string   "price_type"
    t.string   "facilities",                                 array: true
    t.boolean  "sold_out",      default: false
    t.string   "label_tag"
    t.string   "type_package"
    t.integer  "view_count",    default: 0
  end

  add_index "properties", ["user_id"], name: "index_properties_on_user_id", using: :btree

  create_table "property_types", force: :cascade do |t|
    t.string   "name"
    t.string   "ancestry"
    t.string   "slug"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "ancestry_depth", default: 0
  end

  create_table "rates", force: :cascade do |t|
    t.integer  "rater_id"
    t.integer  "rateable_id"
    t.string   "rateable_type"
    t.float    "stars",         null: false
    t.string   "dimension"
    t.text     "review"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "score",         null: false
  end

  add_index "rates", ["rateable_id", "rateable_type"], name: "index_rates_on_rateable_id_and_rateable_type", using: :btree
  add_index "rates", ["rater_id"], name: "index_rates_on_rater_id", using: :btree

  create_table "rating_caches", force: :cascade do |t|
    t.integer  "cacheable_id"
    t.string   "cacheable_type"
    t.float    "avg",            null: false
    t.integer  "qty",            null: false
    t.string   "dimension"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "rating_caches", ["cacheable_id", "cacheable_type"], name: "index_rating_caches_on_cacheable_id_and_cacheable_type", using: :btree

  create_table "regions", id: :bigserial, force: :cascade do |t|
    t.string   "name"
    t.string   "ancestry"
    t.boolean  "featured"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "latitude"
    t.string   "longitude"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.integer  "ancestry_depth",     default: 0
    t.text     "description"
    t.string   "link_url"
  end

  create_table "rent_prices", force: :cascade do |t|
    t.string   "name"
    t.decimal  "price"
    t.string   "price_type"
    t.string   "rent_priceable_type"
    t.integer  "rent_priceable_id"
    t.integer  "position"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  create_table "reports", force: :cascade do |t|
    t.string   "category"
    t.string   "name"
    t.string   "email"
    t.text     "message"
    t.boolean  "status",          default: false
    t.integer  "reportable_id"
    t.string   "reportable_type"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "code"
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name",        null: false
    t.string   "title",       null: false
    t.text     "description", null: false
    t.text     "the_role",    null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "schedule_visits", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.datetime "schedule_date"
    t.datetime "alt_schedule_date"
    t.string   "phone"
    t.string   "email"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "schedule_time"
    t.string   "alt_schedule_time"
    t.integer  "property_id"
  end

  create_table "societies", id: :bigserial, force: :cascade do |t|
    t.string   "title"
    t.boolean  "available"
    t.string   "icon"
    t.string   "societiable_type"
    t.integer  "societiable_id"
    t.integer  "position"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "sold_reports", force: :cascade do |t|
    t.integer  "user_id"
    t.decimal  "price"
    t.string   "sold_reportable_type"
    t.integer  "sold_reportable_id"
    t.datetime "date"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "sold_reports", ["user_id"], name: "index_sold_reports_on_user_id", using: :btree

  create_table "supporteds", force: :cascade do |t|
    t.string   "title"
    t.string   "link"
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  create_table "testimonials", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "position"
    t.integer  "user_id"
    t.text     "message"
    t.boolean  "activated"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "name_company"
  end

  create_table "user_packages", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "package_id"
    t.date     "expire_in"
    t.boolean  "payed",            default: false
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.datetime "confirmation_at"
    t.boolean  "top_listing",      default: false
    t.integer  "max_listing"
    t.integer  "featured_listing"
    t.string   "code"
    t.string   "label_tags"
    t.decimal  "price"
    t.string   "package_name"
  end

  create_table "users", id: :bigserial, force: :cascade do |t|
    t.string   "email",                  default: "",     null: false
    t.string   "encrypted_password",     default: "",     null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,      null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        default: 0,      null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.string   "username",                                null: false
    t.string   "type"
    t.string   "slug"
    t.string   "ancestry"
    t.datetime "deleted_at"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.boolean  "verified"
    t.string   "provider"
    t.string   "uid"
    t.string   "code"
    t.integer  "role_id"
    t.string   "oauth_token"
    t.datetime "oauth_expires_at"
    t.integer  "limit",                  default: 10
    t.date     "expired_at"
    t.string   "package",                default: "free"
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["unlock_token"], name: "index_users_on_unlock_token", unique: true, using: :btree

  create_table "visits", id: :uuid, default: nil, force: :cascade do |t|
    t.uuid     "visitor_id"
    t.string   "ip"
    t.text     "user_agent"
    t.text     "referrer"
    t.text     "landing_page"
    t.integer  "user_id"
    t.string   "referring_domain"
    t.string   "search_keyword"
    t.string   "browser"
    t.string   "os"
    t.string   "device_type"
    t.integer  "screen_height"
    t.integer  "screen_width"
    t.string   "country"
    t.string   "region"
    t.string   "city"
    t.string   "postal_code"
    t.decimal  "latitude"
    t.decimal  "longitude"
    t.string   "utm_source"
    t.string   "utm_medium"
    t.string   "utm_term"
    t.string   "utm_content"
    t.string   "utm_campaign"
    t.datetime "started_at"
  end

  add_index "visits", ["user_id"], name: "index_visits_on_user_id", using: :btree

  create_table "web_settings", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.text     "keywords"
    t.text     "header_tags"
    t.text     "footer_tags"
    t.string   "contact"
    t.string   "email"
    t.string   "favicon_file_name"
    t.string   "favicon_content_type"
    t.integer  "favicon_file_size"
    t.datetime "favicon_updated_at"
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
    t.string   "facebook"
    t.string   "twitter"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.string   "robot"
    t.string   "author"
    t.string   "corpyright"
    t.string   "revisit"
    t.string   "expires"
    t.string   "revisit_after"
    t.string   "geo_placename"
    t.string   "language"
    t.string   "country"
    t.string   "content_language"
    t.string   "distribution"
    t.string   "generator"
    t.string   "rating"
    t.string   "target"
    t.text     "search_engines"
    t.string   "package_title"
    t.text     "package_description"
  end

  create_table "wishlists", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "property_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

end
