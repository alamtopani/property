module ResourceSupport

# # --------------------------Exporter-----------------------
  class Exporter
    def initialize(records)
      @records = records
    end

    def to_csv_property
      csv_string = CSV.generate(col_sep: "\t") do |csv|
        csv << ["Code", "Title","Price", "Activated", 'Featured', "User Email", "Description",
                "Type Property", "Price Type", "Facilities", "Label Tag", "Bathroom",
                "Bedroom", "Parking", "Facing To", "Floor", "Age", "Built Up Area", "Building Area", "Document",
                "Surface Area", "Electricity", "Phone Line", "Garage", "Price In Meter", "Bussines Unit",
                "Price Security", "Price per-Day", "Price per-Month", "Price per-Year", "Width Land",
                "Long Land", "Address", "City", "Province", "Postcode", "Latitude", "Longitude",
                "Subdistrict", "Village", "Komplek", "Start Auction", "End Auction", "Reserve Met", "Financing Considered",
                "Minimum Price", "Increment Per Bid", "Starting Bid"] # headers

        @records.each do |record|
          address = record.address
          basic = record.basic_property
          auction = record.auction
          user_email = record.user.try(:email)
          attr_data = [
                        record.code,
                        record.title,
                        record.price,
                        record.activated,
                        record.featured,
                        user_email,
                        record.description,
                        record.type_property,
                        record.price_type,
                        record.facilities,
                        record.label_tag,
                        basic.bathrooms,
                        basic.bedrooms,
                        basic.parking,
                        basic.facing,
                        basic.floor,
                        basic.age,
                        basic.built_up_area,
                        basic.building_area,
                        basic.document,
                        basic.surface_area,
                        basic.electricity,
                        basic.phone_lines,
                        basic.garage,
                        basic.price_in_meter,
                        basic.business_unit,
                        basic.price_security,
                        basic.price_day,
                        basic.price_month,
                        basic.price_year,
                        basic.width_land,
                        basic.long_land,
                        address.address,
                        address.city,
                        address.province,
                        address.postcode,
                        address.latitude,
                        address.longitude,
                        address.sub_district,
                        address.village,
                        address.area,
                        auction.start_auction,
                        auction.end_auction,
                        auction.reserve_met,
                        auction.financing_considered,
                        auction.minimum_price,
                        auction.increment_per_bid,
                        auction.starting_bid
                    ]
          csv << attr_data
        end
      end
    end

    def to_csv_project
      csv_string = CSV.generate(col_sep: "\t") do |csv|
        csv << ["Code","Title","Description","Price", "Activated", 'Feature',"Developer",
                "Type Project", "Project Size", "Construction", "Possession Start:", "Insights",
                "Facilities", "Address", "City", "Province", "Postcode", "Latitude", "Longitude",
                "Subdistrict", "Village", "Komplek"] # headers
        @records.each do |record|
          address = record.address
          user_email = record.user.try(:email)
          attr_data = [
                        record.code,
                        record.title,
                        record.description,
                        record.price,
                        record.activated,
                        record.featured,
                        user_email,
                        record.type_project,
                        record.project_size,
                        record.construction,
                        record.possession_start,
                        record.insights,
                        record.facilities,
                        address.address,
                        address.city,
                        address.province,
                        address.postcode,
                        address.latitude,
                        address.longitude,
                        address.sub_district,
                        address.village,
                        address.area,
                      ]
          csv << attr_data
        end
      end
    end

  end

end
