require 'zip'
module ImportModel
  ##
  # Export::Property digunakan untuk memproses data mobil
  # dari CSV atau Excel yang diterima dari parameter
  # serta mengolah gambar mobil yang filenya diikut sertakan dalam parameter.
  ##
  class Property
    extend ActiveSupport::Concern
    attr_accessor :file, :columns, :spreadsheet, :errors, :start_row
    attr_accessor :current_row, :current_row_data, :valid_records
    attr_accessor :invalid_records, :car_logos, :file_image, :folder_image

    class << self
      ##
      # unzip_folder:
      #   method yang digunakan untuk mengektrak file archive
      #   yang diterima dari parameter ke folder yang dituju
      #   dan jika foldernya tidak ada maka akan dibuat baru
      ##
      def unzip_folder (file, destination)
        Zip::File.open(file) { |zip_file|
         zip_file.each { |f|
           f_path=File.join(destination, f.name)
           FileUtils.mkdir_p(File.dirname(f_path))
           zip_file.extract(f, f_path) unless File.exist?(f_path)
         }
        }
      end

      ##
      # import_columns:
      #   method yang digunakan sebagai default header dan column position.
      #   method ini akan digunakan pada halaman export untuk memberitahu user tentang
      #   column apa saja yang tersedia serta posisinya pada excel atau csv
      ##
      def import_columns
        {
          code: 0,
          title: 1,
          price: 2,
          activated: 3,
          featured: 4,
          user_email: 5,
          description: 6,
          type_property: 7,
          price_type: 8,
          facilities: 9,
          label_tag: 10,
          bathrooms: 11,
          bedrooms: 12,
          parking: 13,
          facing: 14,
          floor: 15,
          age: 16,
          built_up_area: 17,
          building_area: 18,
          document: 19,
          surface_area: 20,
          electricity: 21,
          phone_lines: 22,
          garage: 23,
          price_in_meter: 24,
          business_unit: 25,
          price_security: 26,
          price_day: 27,
          price_month: 28,
          price_year: 29,
          width_land: 30,
          long_land: 31,
          address: 32,
          city: 33,
          province: 34,
          postcode: 35,
          latitude: 36,
          longitude: 37,
          sub_district: 38,
          village: 39,
          area: 40,
          start_auction: 41,
          end_auction: 42,
          reserve_met: 43,
          financing_considered: 44,
          minimum_price: 45,
          increment_per_bid: 46,
          starting_bid: 47
        }
      end

    end

      #     options = {listing_type: params[:category], file: file_to_hash(:file), file_image:  file_to_hash(:file_image)}
      # options.merge!(params.slice(:listing_type, :start_row, :columns).dup)

    ##
    # Ini adalah method utama yang digunakan untuk membentu suatu object sebelum proses Export.
    # Parameter yang diterima pada args / options adalah:
    #
    #   :file (type: Hash, required: true)
    #     * type: Hash
    #     * required: true
    #     * berisi tentang informasi file csv / excel yang diupload
    #   :file_image (type: Hash, required: false)
    #     * berisi tentang informasi file image yang diupload
    #   :listing_type (type: string, required: true)
    #     * berisi tentang tipe listing yang akan di upload, opsinya: 'other', 'a', 'b'
    #   :start_row (type: number, required: true)
    #     * berisi informasi pada baris keberapa data dari excel akan diproses
    #   :columns (type: Hash, required: true)
    #     * berisi informasi header yang digunakan berserta posisi atau urutan kolom
    #
    # Note:
    #  All options must be symbo, not string.
    # Returns nil if not pushed to Redis or a unique Job ID if pushed.
    #
    # Example:
    #   options = {
    #     :listing_type=>"other",
    #     :file=>{
    #       :path=>"/tmp/RackMultipart20141002-4576-1higmys",
    #       :original_filename=>"A_10.1.150914.xlsx",
    #       :content_type=>"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    #       :headers=>"Content-Disposition: form-data; name=\"file\"; filename=\"A_10.1.150914.xlsx\"\r\nContent-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet\r\n"
    #     },
    #     :file_image=>nil,
    #     :start_row=>"5",
    #     :columns=>{
    #       :make=>"1",
    #       :model=>"2",
    #       :price=>"3"
    #     }
    #   }
    #
    #   Export::Property.new(options)
    ##
    # def initialize(args={})
    #   @file = args[:file]
    #   if args[:file_path].present? && File.exist?(args[:file_path])
    #     @file = File.new(args[:file_path])
    #   end

    #   if args[:file_image_path].present? && File.exist?(args[:file_image_path])
    #     @file_image = File.new(args[:file_image_path])
    #   end

    #   folder_name = rand(Time.now.to_i)*rand(100)
    #   @folder_image = "#{Rails.root}/tmp/#{folder_name}"

    #   @start_row = args[:start_row].to_i
    #   @columns = {}
    #   @valid_records = {}
    #   @invalid_records = {}
    #   @listing_type = args[:listing_type]
    #   @car_logos = Dir.glob("#{Rails.root}/db/fixtures/images/logos/*")
    #   @user_cars = {}

    #   if args[:columns].present?
    #     args[:columns].each do |k, v|
    #       next unless !k.is_a?(String) || !k.is_a?(Symbol)
    #       next if k.blank? || v.blank?
    #       @columns[k.to_sym] = v.to_i
    #     end
    #   end
    # end
    attr_accessor :file, :file_image, :listing_type, :start_row
    def initialize(args={})
      args.each do |args_name, args_value|
        self.send("#{args_name}=", args_value)
      end
      @columns, @valid_records, @invalid_records, @user_cars = {}, {}, {}, {}

      folder_name = rand(Time.now.to_i)*rand(100)
      @folder_image = "#{Rails.root}/tmp/#{folder_name}"

      args[:columns].each do |k, v|
        next unless !k.is_a?(String) || !k.is_a?(Symbol)
        next if k.blank? || v.blank?
        @columns[k.to_sym] = v.to_i
      end if args[:columns].present?
    end



    def start
      @file                = File.new(copy_upload_folder(@file))
      @file_image          = File.new(copy_upload_folder(@file_image)) if @file_image.present?

      return complete_process unless is_valid?
      if @file_image && File.exist?(@file_image.path)
        ImportModel::Property.unzip_folder(@file_image.path, @folder_image)
      end

      @spreadsheet = open_spreadsheet
      @current_row = @start_row.to_i < 2 ? 2 : @start_row.to_i

      while @current_row <= @spreadsheet.try(:last_row).to_i do
        @current_row_data = @spreadsheet.row(@current_row)
        if valid_row?
          options = get_property_params
          options[:category] = @listing_type
          if options[:user_email].blank?
            property = ::Property.new
            property.errors.add(:user, "is not exist with object property #{options}")
            add_errors(property, 'Property')
          else
            options.delete(:user_email)
            options[:facilities] = options[:facilities].split(', ') if options[:facilities].present?
            property = ::Property.new(options)
            property.populate_gallery if property.galleries.blank?
            if property.save
              @valid_records[@current_row] = {row: @current_row_data}
              # upload_images(property)
            else
              add_errors(property,'Property')
            end
          end
        end

        @current_row += 1
      end
      complete_process
    end

    def copy_upload_folder(file)
      new_filename = [Time.now.to_i, File.extname(file.original_filename)].join('')
      path_to_file = "#{Rails.root}/tmp/#{new_filename}"
      FileUtils.cp(file.path, path_to_file)
      return path_to_file
    end

    def complete_process
      if @file && File.exist?(@file.path)
        FileUtils.rm(@file.path)
      end
      # ExportMailer.report(self).deliver
      # web_setting = WebSetting.first || WebSetting.new
      # @user_cars.each do |user_id, user_data|
      #   _cars = ::Property.find(user_data[:cars])
      #   user = ::User.find(user_id)

      #   # user.send :generate_confirmation_token! if !user.confirmed?
      #   # if (['b','b2'].include? @listing_type.to_s.downcase) && user.email.present?
      #   #   ExportMailer.email_dealer(user, _cars, user_data[:password], @listing_type, web_setting).deliver
      #   # elsif @listing_type.to_s.downcase == 'auction' && user.email.present?
      #   #   ExportMailer.email_auction(user, _cars, user_data[:password]).deliver
      #   # end
      # end
      FileUtils.rm_rf(@folder_image)
      FileUtils.rm(@file_image.path) rescue nil if @file_image.present?
    end

    def upload_images(car)
      if get_value(:image_folder).present?
        pending_images = {}
        asset_folder = "#{@folder_image}/#{get_value(:image_folder)}"
        {
          cover:  'Cover',
          image1: 'Image1',
          image2: 'Image2',
          image3: 'Image3',
          image4: 'Image4',
          image5: 'Image5'
        }.each_with_index do |k,v|
          next unless get_value(k.first)
          asset_file = "#{asset_folder}/#{get_value(k.first)}"

          if File.exist?(asset_file)
            gallery = car.galleries.where("lower(title) = ?", k.last.downcase).first
            gallery = car.build_image(k.last, v+1) unless gallery
            begin
              gallery.file = File.open(asset_file)
            rescue
              pending_images[k.first] = asset_file
              car.update_column(:ftp_check, true)
              next
            else
              gallery.save
            end
          else
            # {image_front:  "#{asset_folder}/#{get_value(k.first)}", image_back:  "#{asset_folder}/#{get_value(k.first)}"}
            pending_images[k.first] = asset_file
            car.update_column(:ftp_check, true)
          end

        end
        if car.ftp_check
          FtpWorker.run(car.id, pending_images)
          # FtpWorker.perform_in( (rand(5)+1).minutes , car.id, pending_images)
        end
      end
    end

    def generate_username(email)
      @username = email.split("@").first
      available_username = User.where("lower(username) = ?", @username.downcase)
      if available_username
        @username = @username + rand(1000).to_s
      end
    end

    def valid_row?
      row_errors = []
      row_errors.push('Title is blank') if get_value(:title).blank?
      row_errors.push('Not User Present') if get_value(:user_email).blank?
      return true if row_errors.size == 0
      @invalid_records[@current_row] ||= {row: @current_row_data, errors: []}
      @invalid_records[@current_row][:errors].push(row_errors)
      @invalid_records[@current_row][:errors] = @invalid_records[@current_row][:errors].flatten.uniq
      return false
    end

    def is_valid?
      @errors ||= []
      @errors.push('File is not exist') if @file.blank?
      @errors.push('Header columns can not be defined') if @columns.blank?
      @errors.push('Start row is not integer or defined') if @start_row.blank?

      @errors.blank?
    end

    def add_errors(object, type)
      @invalid_records[@current_row] ||= {row: @current_row_data, errors: []}
      @invalid_records[@current_row][:errors].push(object.errors.full_messages)
      @invalid_records[@current_row][:errors].push("Error on #{type}")
      @invalid_records[@current_row][:errors] = @invalid_records[@current_row][:errors].flatten.uniq
      false
    end

    def get_user_id(email)
      user = Member.find_or_initialize_by(email: email)
      if user.new_record?
        user.username = generate_username(email)
        user.password = 12345678
        user.password_confirmation = 12345678
        user.save
        user.set_free_package
      end
      user.id
    end

    def get_region_id(region_name, parent_id=nil)
      if parent_id.present?
        parent = Region.find(parent_id)
        region = parent.children.find_or_initialize_by(name: region_name)
      else
        region = Region.find_or_initialize_by(name: region_name)
      end
      if region.new_record?
        region.parent_id = parent_id
        region.save
      end
      region.id
    end

    def get_property_params
      province_id = get_region_id(get_value(:province))
      city_id = get_region_id(get_value(:city), province_id)
      sub_district_id = get_region_id(get_value(:sub_district), city_id)
      village_id = get_region_id(get_value(:village), sub_district_id)
      area_id = get_region_id(get_value(:area), village_id)
      activated = get_value(:activated).present? ? get_value(:activated) : true

      {
        code: get_value(:code),
        title: get_value(:title),
        price: get_value(:price),
        activated: activated,
        featured: get_value(:featured),
        user_email: get_value(:user_email),
        description: get_value(:description),
        user_id: get_user_id(get_value(:user_email)),
        type_property: get_value(:type_property),
        started_at: Date.today,
        expired_at: Date.today + 30.days,
        price_type: get_value(:price_type),
        facilities: get_value(:facilities),
        label_tag: get_value(:label_tag),
        address_attributes:{
          address: get_value(:address),
          province_id: province_id,
          city_id: city_id,
          subdistrict_id: sub_district_id,
          village_id: village_id,
          area_id: area_id,
          postcode: get_value(:postcode),
          latitude: get_value(:latitude),
          longitude: get_value(:longitude)
        },
        basic_property_attributes:{
          bathrooms: get_value(:bathrooms),
          bedrooms: get_value(:bedrooms),
          parking: get_value(:parking),
          facing: get_value(:facing),
          floor: get_value(:floor),
          age: get_value(:age),
          built_up_area: get_value(:built_up_area),
          document: get_value(:document),
          width_land: get_value(:width_land),
          long_land: get_value(:long_land),
          surface_area: get_value(:surface_area),
          building_area: get_value(:building_area),
          electricity: get_value(:electricity),
          phone_lines: get_value(:phone_lines),
          garage: get_value(:garage),
          price_in_meter: get_value(:price_in_meter),
          business_unit: get_value(:business_unit),
          price_security: get_value(:price_security),
          price_day: get_value(:price_day),
          price_month: get_value(:price_month),
          price_year: get_value(:price_year)
        },
        auction_attributes:{
          start_auction: get_value(:start_auction),
          end_auction: get_value(:end_auction),
          reserve_met: get_value(:reserve_met),
          financing_considered: get_value(:financing_considered),
          minimum_price: get_value(:minimum_price),
          increment_per_bid: get_value(:increment_per_bid),
          starting_bid: get_value(:starting_bid)
        }
      }
    end

    def get_value(attr)
      return '' if @columns[attr].blank?
      col_index = @columns[attr].to_i - 1
      @current_row_data[col_index].to_s.clean_space.gsub(/(\.0$)|(^\-$)/,'')
    end

    def open_spreadsheet
      case extname_file
        # when '.csv'  then Roo::CSV.new(@file.tempfile.path)
        # when '.xls'  then Roo::Excel.new(@file.tempfile.path, :nil, :ignore)
        # when '.xlsx' then Roo::Excelx.new(@file.tempfile.path, :nil, :ignore)
        when '.csv'  then Roo::CSV.new(@file.path)
        when '.xls'  then Roo::Excel.new(@file.path, :nil, :ignore) rescue false
        when '.xlsx' then Roo::Excelx.new(@file.path, :nil, :ignore)
      else
        raise "Unknown File Type #{file.filename}"
      end
    end

    def extname_file
      # File.extname(@file.filename)
      File.extname(@file.path)
    end

  end
end
