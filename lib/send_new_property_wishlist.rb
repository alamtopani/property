class SendNewPropertyWishlist
  attr_accessor :start_date, :end_date, :duration, :hash_property

  def initialize
    @duration = eval ENV['TIME_GET_DATA']
    @start_date = Date.today - @duration
    @end_date = Date.today
    @hash_property = []
  end

  def send_email!
    village_ids.each do |village_id|
      users_mailers = generate_users_and_property(village_id).first
      village_name = Region.find(village_id).name rescue ''
      properties_mailer = generate_users_and_property(village_id)[1]
      users_mailers.each do |user|
        UserMailer.send_same_property(properties_mailer, user, village_name).deliver
      end if users_mailers.present?
    end
  end

  def ids
    @ids ||= []
  end

  def properties_group_by_village_ids
    @properties_group_by_village_ids ||= properties.group_by  {|a| a.address.village_id}
  end

  def properties_by_village(village_id)
    @properties_ids = Property.joins(:address).activated.by_village_id(village_id)
  end

  def user_ids_from_wishlists(property_ids)
    @user_ids_from_wishlists = Wishlist.where(property_id: property_ids).pluck(:user_id)
  end

  def users(user_ids)
    @users = User.where(id: user_ids)
  end

  def generate_users_and_property(village_id)
    property_by_village = properties_by_village(village_id)
    property_ids = property_by_village.pluck(:id)
    user_ids = user_ids_from_wishlists(property_ids)
    [users(user_ids), property_by_village]
  end

  def village_ids
    @village_ids ||= properties_group_by_village_ids.keys
  end

  def properties
    @properties ||= Property.where("created_at BETWEEN ? AND ?", @start_date, @end_date)
  end

end