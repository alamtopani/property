FactoryGirl.define do
  factory :project do
    title "MyString"
slug "MyString"
code "MyString"
description "MyText"
status "MyString"
activated false
featured false
project_size "MyString"
construction "MyString"
type_project "MyString"
user_id 1
started_at "2015-07-10"
expired_at "2015-07-10"
price "9.99"
possession_start "2015-07-10"
  end

end
